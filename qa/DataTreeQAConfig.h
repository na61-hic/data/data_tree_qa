//
// Created by eugene on 3/27/18.
//

#ifndef DATATREEQA_DATATREEQACONFIG_H
#define DATATREEQA_DATATREEQACONFIG_H

#include <map>
#include <Rtypes.h>
#include <TNamed.h>
#include <cuts/DataTreeCutsConfig.h>

class DataTreeQAConfig : public TNamed {

public:

    DataTreeQAConfig() = default;
    DataTreeQAConfig(const char *name, const char *title = "") : TNamed(name, title) {}
    DataTreeQAConfig(const TString &name, const TString &title = "") : TNamed(name, title) {}


    std::string GetExperiment() const {
        return fExperiment;
    }

    void SetExperiment(const char *fExperiment) {
        DataTreeQAConfig::fExperiment = fExperiment;
    }

    std::string GetSystem() const {
        return fSystem;
    }

    void SetSystem(TString fSystem) {
        fSystem.ToLower();
        DataTreeQAConfig::fSystem = fSystem;
    }

    Double_t GetBeamMomentum() const {
        return fBeamMomentum;
    }

    void SetBeamMomentum(Double_t fBeamMomentum) {
        std::map <TString, std::array<int, 4>> systems = {
          {"pbpb", {208, 82, 208, 82}},
          {"auau", {197, 79, 197, 79}},
          {"xela", {131, 54, 139, 57}},
          {"arsc", { 40, 18,  45, 21}},
        };
        DataTreeQAConfig::fBeamMomentum = fBeamMomentum;
				float mProton = 0.938;
				float elab = sqrt (fBeamMomentum * fBeamMomentum + mProton * mProton);
				float beamRapidity = 0.25 * log ((elab + fBeamMomentum) / (elab - fBeamMomentum));
        std::array <int, 4> systemValues = systems.at(fSystem);
        float a1 = systemValues.at(0);
        float z1 = systemValues.at(1);
        float a2 = systemValues.at(2);
        float z2 = systemValues.at(3);
        DataTreeQAConfig::fRapidityShift = beamRapidity - 0.5 * log (z1 / z2 * a2 / a1);
    }

    Bool_t GetIsSimulation() const {
        return fIsSimulation;
    }

    void SetIsSimulation(Bool_t fIsSimulation) {
        DataTreeQAConfig::fIsSimulation = fIsSimulation;
    }

    cuts::DataTreeCutsConfig *GetCutsConfig() const {
        return fCutsConfig;
    }

    void SetCutsConfig(cuts::DataTreeCutsConfig *fCutsConfig) {
        DataTreeQAConfig::fCutsConfig = fCutsConfig;
    }

    void SetNominalVertex(Double_t x, Double_t y, Double_t z) {
        fNominalVertex[0] = x;
        fNominalVertex[1] = y;
        fNominalVertex[2] = z;
    }

    Double_t GetNominalVertex(Int_t i) const {
        return fNominalVertex[i];
    }

    Double_t GetRapidityShift() const {
        return fRapidityShift;
    }

  Double_t GetPSDEnergyScaleFactor() const {
    return fPSDEnergyScaleFactor;
  }
  void SetPSDEnergyScaleFactor(Double_t f_psd_energy_scale_factor) {
    fPSDEnergyScaleFactor = f_psd_energy_scale_factor;
  }

  Int_t GetMsimMax () {return fMsimMax;}
    Int_t GetMrecoMax () {return fMrecoMax;}
    Int_t GetPSDEnergyMax () {return fPSDEnergyMax;}
    void SetMsimMax (Int_t fMsimMax) {DataTreeQAConfig::fMsimMax = fMsimMax;}
    void SetMrecoMax (Int_t fMrecoMax) {DataTreeQAConfig::fMrecoMax = fMrecoMax;}
    void SetPSDEnergyMax (Double_t fPSDEnergyMax) {DataTreeQAConfig::fPSDEnergyMax = fPSDEnergyMax;}
		
		Long64_t Merge(TCollection* inputlist) {return 1;}

    ClassDef(DataTreeQAConfig, 1);
private:
    std::string fExperiment;
    std::string fSystem;
    Double_t fBeamMomentum;
    Double_t fRapidityShift {0};
    Double_t fMsimMax {-999};
    Double_t fMrecoMax {-999};
    Double_t fPSDEnergyMax {-999.};
    Double_t fPSDEnergyScaleFactor{1.};

    Bool_t fIsSimulation {kTRUE};

    Double_t fNominalVertex[3] = {0, 0, -591.9};

    cuts::DataTreeCutsConfig* fCutsConfig{};

};


#endif //DATATREEQA_DATATREEQACONFIG_H
