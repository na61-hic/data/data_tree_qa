#include <TStyle.h>
#include <TFileCollection.h>
#include <cuts/DataTreeCutsConfig.h>
#include <cuts/DataTreeAbstractCuts.h>
#include <cuts/DataTreeCuts.h>
#include "DataTreeQA.h"

using namespace TMath;
using namespace std;
using namespace qa;

namespace qa {
Int_t GetNGoodTracks(const DataTreeEvent &event, cuts::DataTreeAbstractCuts &fCuts) {
  Int_t nGoodTracks = 0;

  for (Int_t iTrack = 0; iTrack < event.GetNVertexTracks(); iTrack++) {
    if (fCuts.IsGoodTrack(*event.GetVertexTrack(iTrack))) {
      nGoodTracks++;
    }
  }

  return nGoodTracks;
}

Double_t GetRealRapidity(const DataTreeTrack &track, Double_t mass) {
  Double_t energy = Sqrt(Power(track.GetP(), 2.0) + Power(mass, 2.0));
  Double_t pz = track.GetPz();
  return 0.5 * Log((energy + pz) / (energy - pz));
}

Double_t GetRealRapidity(const DataTreeMCTrack &track, Double_t mass) {
  Double_t energy = Sqrt(Power(track.GetP(), 2.0) + Power(mass, 2.0));
  Double_t pz = track.GetPz();
  return 0.5 * Log((energy + pz) / (energy - pz));
}
}

void DataTreeQA::Init(TChain *chain) {
  fChain = chain;
  fChain->SetBranchAddress("DTEvent", &DTEvent);
}

void DataTreeQA::SetReferenceChain(TChain *fReferenceChain) {
  if (fReferenceChain->GetEntries() > 0)
    DataTreeQA::fReferenceChain = fReferenceChain;
  else cout << "Empty reference chain! Using main chain as reference!\n";
}

void DataTreeQA::Run(Long64_t nEvents) {
  Long64_t nEntries = nEvents < fChain->GetEntries() ? nEvents : fChain->GetEntries();
  Long64_t outputStep = nEntries / 10;
  if (outputStep == 0) outputStep = 1;
  Info(__func__, "Events = %lld", nEntries);

  cout << "Beam rapidity = " << fQAConfig.GetRapidityShift() << endl;

  fNGoodEvents = 0;
  for (int i = 0; i < nEntries; i++) {
    if ((i + 1) % outputStep == 0) std::cout << i + 1 << "/" << nEntries << "\r" << std::flush;
    fChain->GetEntry(i);
    Fill_Histograms();
  }

  Info(__func__, "Selected %d/%lld events", fNGoodEvents, nEntries);
}

void DataTreeQA::Init_Histograms() {

  Int_t fNBins;
  fNBins = 1000;

  Double_t p_high = 25.;
  Float_t dcaMax, mcVertexMax, recoVertexMax;
  float vertexChi2max = 100;
  float trackChi2VtxMax = 20;
  int hHitsTPCmax = 250;
  int ndfMax=500;

  float psdXmax, psdYmax;
  fChain->GetEntry(0);
  int nPSDModules = DTEvent->GetNPSDModules();
  int nPSDSections = DTEvent->GetPSDModule(0)->GetNSections();
  float psdModuleScaleFactor = 1.;
  float psdGroupScaleFactor = 1.;
  int minNrun = 0, maxNrun = 1;

  fQAConfig.Dump();
  fCutsConfig->Dump();

  if (fExperiment == "NA61") {
    sTPC = &sTPC_na61;
    dcaMax = 10.;
    recoVertexMax = 10.;
    mcVertexMax = 2.;
    psdXmax = 60.;
    psdYmax = 60.;
    minNrun = 20000;
    maxNrun = 30000;
  } else if (fExperiment == "NA49") {
    sTPC = &sTPC_na61;
    vertexChi2max = 1.1;
    dcaMax = 10.;
    recoVertexMax = 5.;
    mcVertexMax = 1.;
    psdXmax = 160.;
    psdYmax = 160.;
    minNrun = 1389;
    maxNrun = 4102;
    if (fQAConfig.GetBeamMomentum() > 150) {
      psdModuleScaleFactor = 0.03;
      psdGroupScaleFactor = 0.2;
    } else // 40a GeV
    {
      psdModuleScaleFactor = 0.1;
      psdGroupScaleFactor = 0.5;
    }
    if (!fPSDLayoutIsSet)
      SetPsdSubeventLayoutName("NA49_default");
  } else if (fExperiment == "CBM") {
    sTPC = &sTPC_cbm;
    hHitsTPCmax = 13;
    dcaMax = 1.;
    recoVertexMax = 10.;
    mcVertexMax = .5;
    psdXmax = 80.;
    psdYmax = 60.;
    ndfMax = 20;
    if (nPSDModules == 44 && !fPSDLayoutIsSet)
      SetPsdSubeventLayoutName("CBM44_default");
    else if (nPSDModules == 46 && !fPSDLayoutIsSet)
      SetPsdSubeventLayoutName("CBM46_default");
  }

  int nRuns = maxNrun - minNrun + 1;

  Init_Subevents();

  Int_t mSimMax = fQAConfig.GetMsimMax();
  Int_t mRecoMax = fQAConfig.GetMrecoMax();
  Double_t fPSDMax = fQAConfig.GetPSDEnergyMax();

  if (fReferenceChain == nullptr) fReferenceChain = fChain;
  else Info("Initialization", "Using reference chain...");

  if (fQAConfig.GetIsSimulation() && mSimMax < 0) mSimMax = fReferenceChain->GetMaximum("fNumberOfMCTracks") + 50;
  if (mRecoMax < 0) mRecoMax = fReferenceChain->GetMaximum("fNumberOfVertexTracks") + 50;
  if (fPSDMax < 0.) fPSDMax = fReferenceChain->GetMaximum("fPSDEnergy") + 10;

  TString name, title;
  for (Int_t iXyz = 0; iXyz < 3; ++iXyz) {
    name = Form("hVertexPos%s", sXyz[iXyz].Data());
    title = Form("V_{%s}^{reco};V_{%s}^{reco} (cm);Number of events", sXyz[iXyz].Data(), sXyz[iXyz].Data());
    hVertexPos[iXyz] =
        new TH1D(name,
                 title,
                 fNBins,
                 -recoVertexMax + fNominalVertexPos[iXyz],
                 recoVertexMax + fNominalVertexPos[iXyz]);
    name = Form("hMCVertexPos%s", sXyz[iXyz].Data());
    title = Form("V_{%s}^{sim};V_{%s}^{sim} (cm);Number of events", sXyz[iXyz].Data(), sXyz[iXyz].Data());
    hMCVertexPos[iXyz] =
        new TH1D(name, title, fNBins, -mcVertexMax + fNominalVertexPos[iXyz], mcVertexMax + fNominalVertexPos[iXyz]);
    name = Form("hMatchVertexPos%s", sXyz[iXyz].Data());
    title = Form("V_{%s} matching;V_{%s}^{sim} (cm);V_{%s}^{reco} (cm)",
                 sXyz[iXyz].Data(),
                 sXyz[iXyz].Data(),
                 sXyz[iXyz].Data());
    hMatchVertexPos[iXyz] = new TH2D(name,
                                     title,
                                     fNBins,
                                     -mcVertexMax + fNominalVertexPos[iXyz],
                                     mcVertexMax + fNominalVertexPos[iXyz],
                                     fNBins,
                                     -mcVertexMax + fNominalVertexPos[iXyz],
                                     mcVertexMax + fNominalVertexPos[iXyz]);

    name = Form("h2VertexPos%sMult", sXyz[iXyz].Data());
    title =
        Form("V_{%s}^{reco} vs Multiplicity;V_{%s}^{reco} (cm);M_{reco};Counts", sXyz[iXyz].Data(), sXyz[iXyz].Data());
    h2VertexPosMult[iXyz] =
        new TH2D(name,
                 title,
                 fNBins,
                 -recoVertexMax + fNominalVertexPos[iXyz],
                 recoVertexMax + fNominalVertexPos[iXyz],
                 Nint(mRecoMax),
                 0,
                 mRecoMax);

    name = Form("hTrackDCA%s", sXyz[iXyz].Data());
    title = Form("DCA_{%s};DCA_{%s}^{sim} (cm);Number of tracks", sXyz[iXyz].Data(), sXyz[iXyz].Data());
    hTrackDCA[iXyz] = new TH1D(name, title, fNBins, -dcaMax, dcaMax);
  }

  if (fCutsConfig && fCutsConfig->IsApplyEventVertex())
    vertexChi2max = fCutsConfig->GetVertexChi2Max();
  hVertexChi2 = new TH1D("hVertexChi2", "Vertex #chi^{2};Vertex #chi^{2};Number of events", 500, 0, vertexChi2max);

  hTrackDCAXY = new TH2D("hTrackDCAXY",
                         "DCA_{y} : DCA_{x};DCA_{x} (cm);DCA_{y} (cm)",
                         fNBins,
                         -dcaMax,
                         dcaMax,
                         fNBins,
                         -dcaMax,
                         dcaMax);

  hMreco = new TH1D("hMreco", "Number of reconstructed tracks;M_{reco};Number of events", Nint(mRecoMax), 0, mRecoMax);
  hE = new TH1D("hE", "PSD energy;E_{PSD} (GeV);Number of events", 500, 0, fPSDMax);
  if (TMath::Abs(fQAConfig.GetPSDEnergyScaleFactor() - 1.0) > 1.e-4) {
    hE->SetXTitle(Form("E_{PSD} (GeV) #times %.3f", fQAConfig.GetPSDEnergyScaleFactor()));
  }

  hMEcorr = new TH2D("hMEcorr",
                     "E_{PSD} : Multiplicity;Multiplicity;E_{PSD} (GeV)",
                     Nint(mRecoMax),
                     0,
                     mRecoMax,
                     500,
                     0,
                     fPSDMax);
  if (fQAConfig.GetIsSimulation()) {
    hPsiRP = new TH1F("hPsiRP", "Reaction plane angle;#Psi_{RP} (rad);Number of events", 500, 0., 2 * TMath::Pi());
    hB = new TH1F("hB", "Impact parameter;B (fm);Number of events", 500, 0., 20.);
    hMEMCcorr = new TH2D("hMEMcorr",
                         "E_{PSD}^{sim} : Multiplicity;Multiplicity;E_{PSD}^{sim} (GeV)",
                         Nint(mRecoMax),
                         0,
                         mRecoMax,
                         500,
                         0,
                         200);

    for (uint i = 0; i < MCnames.size(); i++) {
      hMsim.at(i) = new TH1D("hMsim_" + MCnames[i],
                             Form("Number of %s MC tracks;M_{%s};Number of events",
                                  MCnames[i].Data(),
                                  MCnames[i].Data()),
                             Nint(mSimMax) * 10,
                             0,
                             mSimMax * 10);
      hMMcorr.at(i) = new TH2D("hMMcorr_" + MCnames[i],
                               Form("Mreco : M_{%s};M_{%s};M_{reco}", MCnames[i].Data(), MCnames[i].Data()),
                               Nint(mSimMax) * 10,
                               0,
                               mSimMax * 10,
                               Nint(mRecoMax),
                               0,
                               mRecoMax);
    }
    hMBcorr = new TH2D("hMBcorr",
                       "Multiplicity vs impact parameter;B (fm);Multiplicity",
                       200,
                       0,
                       20,
                       Nint(mRecoMax),
                       0,
                       mRecoMax);
    hEBcorr = new TH2D("hEBcorr", "E_{PSD} vs impact parameter;B (fm);E_{PSD} (GeV)", 200, 0, 20, 500, 0, fPSDMax);
    hEEcorr = new TH2D("hEEcorr",
                       "E_{PSD}^{reco} vs E_{PSD}^{sim};E_{PSD}^{sim} (GeV);E_{PSD}^{reco} (GeV)",
                       500,
                       0,
                       200,
                       500,
                       0,
                       fPSDMax);
  }
  hNdf = new TH1D("hNdf", "Track NDF;NDF;Number of tracks", ndfMax, 0, ndfMax);

  Double_t trackChi2Ndf_Max = fCutsConfig->GetTrackChiMax();
  if (trackChi2Ndf_Max == 999) trackChi2Ndf_Max = 20;
  hChi2Ndf = new TH1D("hChi2Ndf", "Track #chi^{2}/NDF;#chi^{2}/NDF;Number of tracks", 500, 0, trackChi2Ndf_Max);
  hCharge = new TH1D("hCharge", "Track Charge;Track Charge;Number of tracks", 5, -2, 2);
  
  if (fCutsConfig && fCutsConfig->IsApplyTrackQuality())
    trackChi2VtxMax = fCutsConfig->GetTrackChiVtxMax();
  hChi2Vertex = new TH1D("hChi2Vertex", "Vertex #chi^{2};#chi^{2};Number of tracks", 500, 0, trackChi2VtxMax);

  /* PSD histogram initialization */
  hPSDGroupsCorr.reserve(gPSDCorr.size());
  for (auto &correlation : gPSDCorr) {
    auto group1 = correlation.first;
    auto group2 = correlation.second;
    if (!(group1 && group2)) {
      // skipping empty correlation
      Warning(__func__, "Skipping empty subgroup definition");
      hPSDGroupsCorr.push_back(nullptr);
      continue;
    }

    name = "hPSDGroupsCorr_" + group1->short_name() + "_" + group2->short_name();
    title = "hPSDGroupsCorr_" + group1->short_name() + "_" + group2->short_name();
    hPSDGroupsCorr.push_back(new TH2D(name.Data(),
                                      name.Data(),
                                      fNBins,
                                      0,
                                      psdGroupScaleFactor * fPSDMax,
                                      fNBins,
                                      0,
                                      psdGroupScaleFactor * fPSDMax));
    hPSDGroupsCorr.back()->SetXTitle(Form("E_{%s} (GeV)", group1->displayName.Data()));
    hPSDGroupsCorr.back()->SetYTitle(Form("E_{%s} (GeV)", group2->displayName.Data()));
  }

  h2PSDModuleEnergy = new TH2D("h2PSDModuleEnergy", "Module energy:Module Number;Module number;E_{mod} (GeV)",
                               nPSDModules, 0, nPSDModules, fNBins, 0., psdModuleScaleFactor * fPSDMax);
  p2PSDModulePositionEnergy =
      new TProfile2D("p2PSDModulePositionEnergy", "Module mean energy : Module Position;y (cm); x (cm);",
                     150, -psdXmax, psdXmax,
                     150, -psdYmax, psdYmax);

  hPSDMultCorr.reserve(gPSDGroups.size());
  for (const auto &group : gPSDGroups) {
    if (!group) {
      // skipping empty PSD group
      Warning(__func__, "Skipping empty subgroup definition");
      hPSDMultCorr.push_back(nullptr);
      continue;
    }
    name = "hPSDMultCorr_" + group->short_name();
    hPSDMultCorr.push_back(new TH2D(name,
                                    name,
                                    Nint(mRecoMax),
                                    0,
                                    Nint(mRecoMax),
                                    fNBins,
                                    0,
                                    psdGroupScaleFactor * fPSDMax));
    hPSDMultCorr.back()->SetXTitle("Multiplicity");
    hPSDMultCorr.back()->SetYTitle(Form("E_{%s} (GeV)", group->displayName.Data()));
  }

  for (uint iTPC = 0; iTPC < sTPC->size(); iTPC++) {
    name = Form("hNhits_%s", sTPC->at(iTPC).Data());
    title = Form("hNhits_%s;Number of hits in %s;Number of tracks", sTPC->at(iTPC).Data(), sTPC->at(iTPC).Data());
    hNHits.at(iTPC) = new TH1D(name, title, hHitsTPCmax, 0, hHitsTPCmax);

    name = Form("hNhitsPot_%s", sTPC->at(iTPC).Data());
    title =
        Form("hNhits_%s;Number of potential hits in %s;Number of tracks", sTPC->at(iTPC).Data(), sTPC->at(iTPC).Data());
    hNHitsPot.at(iTPC) = (TH1D *) hNHits[iTPC]->Clone(name);
    hNHitsPot[iTPC]->SetTitle(title.Data());

    name = Form("hRatioNhitsNhitsPot_%s", sTPC->at(iTPC).Data());
    hRatioNHitsNHitsPot.at(iTPC) = new TH1D(name, ";N hits / N potential hits", 200, 0.0, 2.0);

    name = Form("hNhitsNHitsPot_%s", sTPC->at(iTPC).Data());
    hNHitsNHitsPot.emplace(iTPC, new TH2D(name, name, hHitsTPCmax, 0, hHitsTPCmax, hHitsTPCmax, 0, hHitsTPCmax));
    hNHitsNHitsPot[iTPC]->SetXTitle("Number of potential hits");
    hNHitsNHitsPot[iTPC]->SetYTitle("Number of hits");


    name = Form("hTrackdEdx_%s", sTPC->at(iTPC).Data());
    hTrackdEdx.at(iTPC) = new TH2D(name, name, fNBins, -10, 10, 10 * fNBins, 0, 10);
    hTrackdEdx[iTPC]->SetXTitle("q * log(20 #times p)");
    hTrackdEdx[iTPC]->SetYTitle("dE/dx (MIP)");

    name = Form("hTrackdEdxRST_%s", sTPC->at(iTPC).Data());
    hTrackdEdxRST.at(iTPC) = dynamic_cast<TH2D *>(hTrackdEdx[iTPC]->Clone(name));
    name = Form("hTrackdEdxWST_%s", sTPC->at(iTPC).Data());
    hTrackdEdxWST.at(iTPC) = dynamic_cast<TH2D *>(hTrackdEdx[iTPC]->Clone(name));
  }

  for (Int_t iMom = 0; iMom < kAxes; ++iMom) {
    auto axis = gMomentumAxes[iMom];
    name = "hTrack" + axis.name;
    hTrackMomentum.at(iMom) = new TH1D(name, name, axis.nBins, axis.min, axis.max);
    hTrackMomentum[iMom]->SetXTitle(axis.displayName);
    hTrackMomentum[iMom]->SetYTitle("Number of tracks");
    if (fQAConfig.GetIsSimulation() && fAnalyzeTrackMatching) {
      name = Form("hTrackMatch%s", axis.name.Data());
      hTrackMomentumMatch.at(iMom) = new TH2D(name, name, axis.nBins, axis.min, axis.max, axis.nBins, axis.min, axis.max);
      hTrackMomentumMatch[iMom]->SetTitle(axis.displayName);
      hTrackMomentumMatch[iMom]->SetXTitle(Form("%s (reco)", axis.displayName.Data()));
      hTrackMomentumMatch[iMom]->SetYTitle(Form("%s (sim)", axis.displayName.Data()));

      name = Form("hTrackMatchPositive%s", axis.name.Data());
      hTrackMomentumMatchPositive.at(iMom) = (TH2D *) hTrackMomentumMatch[iMom]->Clone(name);

      name = Form("hTrackMatchNegative%s", axis.name.Data());
      hTrackMomentumMatchNegative.at(iMom) = (TH2D *) hTrackMomentumMatch[iMom]->Clone(name);

      name = "h2TrackResolution" + axis.name;
      h2TrackResolution.at(iMom) =
          new TH2D(name, axis.displayName + "  resolution", axis.nBins, axis.min, axis.max, 1000, -0.3, 0.3);
      h2TrackResolution[iMom]->SetXTitle(axis.displayName);

      TString axisTitle;
      if (iMom == kPT || iMom == kP || iMom == kPX || iMom == kPZ || iMom == kPZ)
        axisTitle = "#Delta " + axis.displayName + "/" + axis.displayName;
      else
        axisTitle = "#Delta " + axis.displayName;
      h2TrackResolution[iMom]->SetYTitle(axisTitle);

      name = "h2TrackResolutionPositive" + axis.name;
      h2TrackResolutionPositive.at(iMom) = (TH2D *) h2TrackResolution[iMom]->Clone(name);

      name = "h2TrackResolutionNegative" + axis.name;
      h2TrackResolutionNegative.at(iMom) = (TH2D *) h2TrackResolution[iMom]->Clone(name);

      name = "hTrackResolution" + axis.name;
      hTrackResolution.at(iMom) = new TH1D(name, axis.displayName + "  resolution", 1000, -0.3, 0.3);
      hTrackResolution[iMom]->SetXTitle(axisTitle);
      hTrackResolution[iMom]->SetYTitle("Number of tracks");

      name = "hTrackResolutionPositive" + axis.name;
      hTrackResolutionPositive.at(iMom) = (TH1D *) hTrackResolution[iMom]->Clone(name);

      name = "hTrackResolutionNegative" + axis.name;
      hTrackResolutionNegative.at(iMom) = (TH1D *) hTrackResolution[iMom]->Clone(name);

      name = "pTrackResolution" + axis.name;
      title = Form("pTrackResolution%s;%s;#delta%s/%s",
                   axis.name.Data(),
                   axis.displayName.Data(),
                   axis.displayName.Data(),
                   axis.displayName.Data());
      pTrackResolution.at(iMom) = new TProfile(name, title, axis.nBins, axis.min, axis.max);
      pTrackResolution[iMom]->SetMaximum(1.);

      name = "pTrackResolutionPositive" + axis.name;
      title = Form("pTrackResolutionPositive%s;%s;#delta%s/%s",
                   axis.name.Data(),
                   axis.displayName.Data(),
                   axis.displayName.Data(),
                   axis.displayName.Data());
      pTrackResolutionPositive.at(iMom) = (TProfile *) pTrackResolution[iMom]->Clone(name);
      pTrackResolutionPositive[iMom]->SetTitle(title);
      pTrackResolutionPositive[iMom]->SetMaximum(1.);

      name = "pTrackResolutionNegative" + axis.name;
      title = Form("pTrackResolutionNegative%s;%s;#delta%s/%s",
                   axis.name.Data(),
                   axis.displayName.Data(),
                   axis.displayName.Data(),
                   axis.displayName.Data());
      pTrackResolutionNegative.at(iMom) = (TProfile *) pTrackResolution[iMom]->Clone(name);
      pTrackResolutionNegative[iMom]->SetTitle(title);
      pTrackResolutionNegative[iMom]->SetMaximum(1.);
    }
  }
  for (int mom = 0; mom < 4; ++mom) {
    const Int_t *axes = gTH2Axes[mom];

    auto xAxis = gMomentumAxes[axes[0]];
    auto yAxis = gMomentumAxes[axes[1]];

    TString name = "hTrack" + xAxis.name + yAxis.name;
    TString xTitle = xAxis.displayName;
    TString yTitle = yAxis.displayName;
    hTrackMomentumCorr.at(mom) = new TH2D(name, "all",
                                          xAxis.nBins, xAxis.min, xAxis.max,
                                          yAxis.nBins, yAxis.min, yAxis.max
    );
    hTrackMomentumCorr[mom]->SetXTitle(xTitle);
    hTrackMomentumCorr[mom]->SetYTitle(yTitle);
    if (fQAConfig.GetIsSimulation()) {
      for (uint motherId = 0; motherId < MCnames.size(); motherId++) {
        name = Form("%s_%s", hTrackMomentumCorr[mom]->GetName(), MCnames[motherId].Data());
        name.ReplaceAll("hTrack", "hMCTrack");
        hMCTrackMomentumCorr[mom].at(motherId) = (TH2D *) hTrackMomentumCorr[mom]->Clone(name);
        hMCTrackMomentumCorr[mom][motherId]->SetTitle(MCnames[motherId].Data());
      }
    }
  }
  
  if (fQAConfig.GetIsSimulation()) {
    hTrackMatchCharge = new TH1D("hTrackMatchCharge", "q_{rec} x q_{sim};q_{rec} x q_{sim};Number of tracks", 4, -2, 2);
    pMatchingEfficiency =
        new TProfile("pMatchingEfficiency", "M_{matched} / M_{reco};Number of events", 100, 0.0, mRecoMax);

    for (Int_t iHarm = 0; iHarm < 2; ++iHarm) {
      for (Int_t iPart = 0; iPart < kParticles; ++iPart) {
        auto &particle = gParticles[iPart];
        name = Form("hv%d_pT_%s", iHarm + 1, particle.name.Data());
        title = Form("hv%d_pT_%s;p_{T} (GeV/#it{c});v_{%d}", iHarm + 1, particle.name.Data(), iHarm + 1);
        pVn_pT[iHarm].at(iPart) = new TProfile(name, name, 50, gMomentumAxes[kPT].min, gMomentumAxes[kPT].max);
        name = Form("hv%d_Y_%s", iHarm + 1, particle.name.Data());
        title = Form("hv%d_Y_%s;#it{y};v_{%d}", iHarm + 1, particle.name.Data(), iHarm + 1);
        pVn_Y[iHarm].at(iPart) = new TProfile(name, name, 50, gMomentumAxes[kYM].min, gMomentumAxes[kYM].max);
      }
    }
#ifdef DATATREE_CBM    
    if (fAnalyzeDileptons) {
      for (auto dilepton : gDileptons)
        hMinv_ee.emplace(dilepton.pdg, new TH1F (Form("hMinv_%s",dilepton.name.c_str()), Form("hMinv_%s;M_{ee} (GeV/#it{c}^{2});dN/dM_{ee}", dilepton.title.c_str()), 500, 0, 1.5));
    }
#endif      
  }
    
  for (int iV0 = 0; iV0 < DTEvent->GetNV0Types(); iV0++) {
    int pdg = DTEvent->GetNV0Pdg(iV0);
    auto particle = GetParticle(pdg);

    name = "hMinv_" + particle->name + "_TOF";
    title = "M_{inv} for " + particle->displayName + " (TOF);M_{inv} (GeV/c^{2});Number of candidates";
    hV0candMinvTOF.emplace(pdg, new TH1F(name, title, 500, particle->mass * 0.85, particle->mass * 1.15));
    name = "h2MinvPt_" + particle->name + "_TOF";
    title = "M_{inv} : p_{T} for " + particle->displayName
        + " (TOF);p_{T} (GeV/c);M_{inv} (GeV/c^{2});Number of candidates";
    h2V0candMinvPtTOF.emplace(pdg,
                              new TH2F(name,
                                       title,
                                       gMomentumAxes[kPT].nBins,
                                       gMomentumAxes[kPT].min,
                                       gMomentumAxes[kPT].max,
                                       500,
                                       particle->mass * 0.85,
                                       particle->mass * 1.15));
    name = "h2MinvRap_" + particle->name + "_TOF";
    title =
        "M_{inv} : #it{y} for " + particle->displayName + " (TOF);#it{y};M_{inv} (GeV/c^{2});Number of candidates";
    h2V0candMinvRapTOF.emplace(pdg,
                               new TH2F(name,
                                        title,
                                        gMomentumAxes[kYM].nBins,
                                        gMomentumAxes[kYM].min,
                                        gMomentumAxes[kYM].max,
                                        500,
                                        particle->mass * 0.85,
                                        particle->mass * 1.15));
    if (fQAConfig.GetIsSimulation()) {
      hV0candMinvMC.emplace(pdg,
                            (TH1F *) hV0candMinvTOF.at(pdg)->Clone(((TString) hV0candMinvTOF.at(pdg)->GetName()).ReplaceAll(
                                "TOF",
                                "MC")));
      hV0candMinvMC.at(pdg)->SetTitle(((TString) hV0candMinvTOF.at(pdg)->GetTitle()).ReplaceAll("TOF", "MC"));
      h2V0candMinvPtMC.emplace(pdg,
                               (TH2F *) h2V0candMinvPtTOF.at(pdg)->Clone(((TString) h2V0candMinvPtTOF.at(pdg)->GetName()).ReplaceAll(
                                   "TOF",
                                   "MC")));
      h2V0candMinvPtMC.at(pdg)->SetTitle(((TString) h2V0candMinvPtTOF.at(pdg)->GetTitle()).ReplaceAll("TOF", "MC"));
      h2V0candMinvRapMC.emplace(pdg,
                                (TH2F *) h2V0candMinvRapTOF.at(pdg)->Clone(((TString) h2V0candMinvRapTOF.at(pdg)->GetName()).ReplaceAll(
                                    "TOF",
                                    "MC")));
      h2V0candMinvRapMC.at(pdg)->SetTitle(((TString) h2V0candMinvRapTOF.at(pdg)->GetTitle()).ReplaceAll("TOF", "MC"));
    }
  }

  for (Int_t iPart = 0; iPart < kParticles; ++iPart) {
    auto particle = gParticles[iPart];

    auto xAxis = gMomentumAxes[kYM];
    auto yAxis = gMomentumAxes[kPT];

    name = Form("hTrackPtY_%s", particle.name.Data());
    hTrackPtY.at(iPart) =
        new TH2D(name, particle.displayName, xAxis.nBins, xAxis.min, xAxis.max, yAxis.nBins, yAxis.min, yAxis.max);
    hTrackPtY[iPart]->SetXTitle(xAxis.displayName);
    hTrackPtY[iPart]->SetYTitle(yAxis.displayName);
    if (fQAConfig.GetIsSimulation()) {
      for (uint motherId = 0; motherId < MCnames.size(); motherId++) {
        name = Form("%s_%s", hTrackPtY[iPart]->GetName(), MCnames[motherId].Data());
        name.ReplaceAll("hTrack", "hMCTrack");
        hMCTrackPtY[iPart].at(motherId) = (TH2D *) hTrackPtY[iPart]->Clone(name);
        hMCTrackPtY[iPart][motherId]->SetTitle(Form("%s_{%s}", hTrackPtY[iPart]->GetTitle(), MCnames[motherId].Data()));
      }
    }
    Int_t bins[] = {100, 100, 100, gMultiplicity.nBins};
    Double_t mins[] = {gMomentumAxes[kPT].min, gMomentumAxes[kYM].min, gMomentumAxes[kPHI].min, gMultiplicity.min};
    Double_t maxs[] = {gMomentumAxes[kPT].max, gMomentumAxes[kYM].max, gMomentumAxes[kPHI].max, gMultiplicity.max};
    if (fAnalyzeEfficiency) {
      name = Form("hTrack_%s_pT_Y_phi_Mult", particle.name.Data());
      hTrackPtYPhiMult.emplace(iPart, new THnSparseD(name, name, 4, bins, mins, maxs));
      hTrackPtYPhiMult[iPart]->GetAxis(0)->SetTitle(gMomentumAxes[kPT].displayName);
      hTrackPtYPhiMult[iPart]->GetAxis(1)->SetTitle(gMomentumAxes[kYM].displayName);
      hTrackPtYPhiMult[iPart]->GetAxis(2)->SetTitle(gMomentumAxes[kPHI].displayName);
      hTrackPtYPhiMult[iPart]->GetAxis(3)->SetTitle(gMultiplicity.displayName);

      name = Form("hMCTrack_%s_pT_Y_phi_Mult", particle.name.Data());
      hMCTrackPtYPhiMult.emplace(iPart, dynamic_cast<THnSparseD *>(hTrackPtYPhiMult[iPart]->Clone(name)));
    }

    name = Form("hYield_%s", particle.name.Data());
    title = Form("%s yield;B(fm);Yield", particle.name.Data());
    hYields.at(iPart) = new TProfile(name, title, 100, 0, 20);
  }

  Int_t bins[] = {100, 100, 100, gMultiplicity.nBins};
  Double_t mins[] = {gMomentumAxes[kPT].min, gMomentumAxes[kETA].min, gMomentumAxes[kPHI].min, gMultiplicity.min};
  Double_t maxs[] = {gMomentumAxes[kPT].max, gMomentumAxes[kETA].max, gMomentumAxes[kPHI].max, gMultiplicity.max};
  if (fAnalyzeEfficiency) {
    name = Form("hTrack_hadplus_pT_Eta_phi_Mult");
    hTrackPtEtaPhiMultPositive = new THnSparseD(name, name, 4, bins, mins, maxs);
    hTrackPtEtaPhiMultPositive->GetAxis(0)->SetTitle(gMomentumAxes[kPT].displayName);
    hTrackPtEtaPhiMultPositive->GetAxis(1)->SetTitle(gMomentumAxes[kETA].displayName);
    hTrackPtEtaPhiMultPositive->GetAxis(2)->SetTitle(gMomentumAxes[kPHI].displayName);
    hTrackPtEtaPhiMultPositive->GetAxis(3)->SetTitle(gMultiplicity.displayName);

    name = Form("hTrack_hadminus_pT_Eta_phi_Mult");
    hTrackPtEtaPhiMultNegative = (THnSparse *) hTrackPtEtaPhiMultPositive->Clone(name);
    name = Form("hMCTrack_hadplus_pT_Eta_phi_Mult");
    hMCTrackPtEtaPhiMultPositive = new THnSparseD(name, name, 4, bins, mins, maxs);
    name = Form("hMCTrack_hadminus_pT_Eta_phi_Mult");
    hMCTrackPtEtaPhiMultNegative = (THnSparse *) hTrackPtEtaPhiMultPositive->Clone(name);
  }

  hTofLength = new TH1F("hTofLength","TOF hit path length;L (cm);Number of hits", 
    500, fCutsConfig->GetTofHitPathLengthLimits().at(0), fCutsConfig->GetTofHitPathLengthLimits().at(1));
  hTofTime = new TH1F("hTofTime","TOF hit time;Time (?);Number of hits", 
    500, fCutsConfig->GetTofHitTimeLimits().at(0), fCutsConfig->GetTofHitTimeLimits().at(1));
  h2TofXY = new TH2F("h2TofXY","TOF hit Y:X;X_{hit} (cm);Y_{hit} (cm);Number of hits", 
    500, fCutsConfig->GetTofHitPositionLimits().at(0), fCutsConfig->GetTofHitPositionLimits().at(1),
    500, fCutsConfig->GetTofHitPositionLimits().at(2), fCutsConfig->GetTofHitPositionLimits().at(3));
  hTofZ = new TH1F("hTofZ","TOF hit Z;Z_{hit} (cm);Number of hits", 
    500, fCutsConfig->GetTofHitPositionLimits().at(4), fCutsConfig->GetTofHitPositionLimits().at(5));
  hTofMatchCharge = new TH1I("hTofMatchCharge", "q_{hit} x q_{track};q_{hit} x q_{track};Number of hits", 2,-1.1,1.1);
  hTofMatchRadius = new TH1F("hTofMatchRadius","TOF hit match radius;R_{match} (cm);Number of hits", 
    500, 0., fCutsConfig->GetTofHitMatchRadius());
  p2TofMatchRadiusXY = new TProfile2D("p2TofMatchRadiusXY","TOF hit match radius:Y:X;X_{hit} (cm);Y_{hit} (cm);R_{match} (cm)", 
    500, fCutsConfig->GetTofHitPositionLimits().at(0), fCutsConfig->GetTofHitPositionLimits().at(1),
    500, fCutsConfig->GetTofHitPositionLimits().at(2), fCutsConfig->GetTofHitPositionLimits().at(3));
  pTofMatchRadiusZ = new TProfile("pTofMatchRadiusZ","TOF hit match radius:Z;Z_{hit} (cm);R_{match} (cm)", 
    500, fCutsConfig->GetTofHitPositionLimits().at(4), fCutsConfig->GetTofHitPositionLimits().at(5));
  h2TofM2 = new TH2F("h2TofM2",
                    "m^2 all;q x p (GeV/#it{c});m^{2} (GeV^{2}/#it{c}^{4})",
                    fNBins,
                    -p_high,
                    p_high,
                    fNBins,
                    -2.,
                    6.);
  h2TofBeta = new TH2F("h2TofBeta", "#beta_{TOF};q x p (GeV/#it{c});#beta", fNBins, -p_high, p_high, fNBins, 0, 1.1);

  for (Int_t iPart = 0; iPart < kLAMBDA; ++iPart) {
    name = Form("h2TofM2_%s", gParticles[iPart].name.Data());
    title = Form("m^{2}:q x p (%s);q x p (GeV/#it{c});m^{2} (GeV^{2}/#it{c}^{4})", gParticles[iPart].displayName.Data());
    int pdg = gParticles[iPart].pdg;
    h2TofM2Part.emplace(pdg, (TH2F*)h2TofM2->Clone(name));
    h2TofM2Part.at(pdg)->SetTitle(title);
  }

  hTrackCutsEfficiency =
      new TH2D("hTrackCutsEfficiency",
               ";M_{total};M_{Good}",
               Nint(mRecoMax),
               0,
               Nint(mRecoMax),
               Nint(mRecoMax),
               0,
               Nint(mRecoMax));

  if (fAnalyzePSDModByMod) {
    psdDirectory = new TDirectory("psd", "PSD QA");
    psdDirectory->cd();

    TH2D h2PSDvsMult_proto
        ("h2PSDvsMult_proto", ";M_{trk};E (GeV)", Nint(mRecoMax), 0, Nint(mRecoMax), fNBins, 0, fPSDMax / 3);
    TH2D h2PSDRatioVsMult_proto
        ("h2PSDRatioVsMult_proto", ";M_{trk};E_{mod}/E_{all}", Nint(mRecoMax), 0, Nint(mRecoMax), fNBins, 0, 1.);

    h2PSDvsMultiplicity.resize(nPSDModules, nullptr);
    h2PSDRatioVsMultiplicity.resize(nPSDModules, nullptr);
    for (int im = 0; im < nPSDModules; ++im) {
      h2PSDvsMultiplicity.at(im) =
          dynamic_cast<TH2D *>(h2PSDvsMult_proto.Clone(Form("h2PSDvsMultiplicity_%d", im + 1)));
      h2PSDvsMultiplicity[im]->SetTitle(Form("Module %d", im + 1));

      h2PSDRatioVsMultiplicity.at(im) =
          dynamic_cast<TH2D *>(h2PSDRatioVsMult_proto.Clone(Form("h2PSDRatioVsMultiplicity_%d", im + 1)));
      h2PSDRatioVsMultiplicity[im]->SetTitle(Form("Module %d", im + 1));
    }

//    for (int im : {17, 18, 20, 21, 23, 24, 26, 27}) {
//      auto h2PSDvsMultiplicity_1_5 =
//          dynamic_cast<TH2D *>(h2PSDvsMult_proto.Clone(Form("h2PSDvsMultiplicity_1_5_%d", im + 1)));
//      auto h2PSDvsMultiplicity_6_10 =
//          dynamic_cast<TH2D *>(h2PSDvsMult_proto.Clone(Form("h2PSDvsMultiplicity_6_10_%d", im + 1)));
//      h2PSDvsMultiplicityLongitudinal.emplace(im, make_pair(h2PSDvsMultiplicity_1_5, h2PSDvsMultiplicity_6_10));
//    }
  }



  // Run-by-run histograms
  if (!fQAConfig.GetIsSimulation()) {
    for (int iBPD = 0; iBPD < 3; ++iBPD) {
      for (int iXY = 0; iXY < 2; ++iXY) {
        hBeamPosition[iBPD].at(iXY) = new TH1D(Form("hBeamPosition_BPD%d_%d", iBPD, iXY), ";(cm)", 200, -10., 10.);
      }
    }
    hWFABeamHits = new TH1D("hWFABeamHits", "", 1000, -30000, 30000);
    hS1S2 = new TH2D("hS1S2", ";S1 signal; S2 signal",
                     500, 0, 500, 500, 0, 500);
    hS1 = new TH1D("hS1", ";signal", 500, 0, 500);
    hS2 = dynamic_cast<TH1D *>(hS1->Clone("hS2"));
  }

  if (!fQAConfig.GetIsSimulation() && fAnalyzeRunByRun) {
    runByRunDirectory = new TDirectory("run_by_run", "Run-by-run QA");
    runByRunDirectory->cd();

    hNEventsInRun = new TH1I("hNEventsInRun", "", nRuns, minNrun, maxNrun);
    hNGoodEventsInRun = (TH1I *) hNEventsInRun->Clone("hNGoodEventsInRun");

    hNGoodTracksByRun = new TProfile("hNGoodTracksByRun", "", nRuns, minNrun, maxNrun);
    hNTracksByRun = (TProfile *) hNGoodTracksByRun->Clone("hNTracksByRun");
    hPSDEnergyByRun = (TProfile *) hNGoodTracksByRun->Clone("hPSDEnergyByRun");

    for (int i = 0; i < 3; i++) {
      hVertexPosVsRun.at(i) =
          (TProfile *) hNTracksByRun->Clone(((std::string) "hVertexPosByRun_" + sXyz[i].Data()).c_str());
    }

//    for (auto trigger : gTriggerList) {
//      hTriggerByRun.at(trigger) = (TProfile *) hNTracksByRun->Clone(Form("hTriggerByRun_S%d", trigger));
//    }

    for (int k = 0; k < 3; ++k) {
      for (int i = 0; i < 2; ++i) {
        name = Form("hBeamPosition_BPD%d_%s", k, sXyz[i].Data());
        pBeamPositionVsRun[k].at(i) = (TProfile *) hNTracksByRun->Clone(name);
      }
    }

    hPSDModuleEnergyByRun.resize(nPSDModules, nullptr);
    for (int l = 0; l < nPSDModules; ++l) {
      name = Form("hPSDModuleEnergyByRun_mod%d", l + 1);
      hPSDModuleEnergyByRun.at(l) = (TProfile *) hNTracksByRun->Clone(name);
    }

    runByRunPSDResponseMod2Directory = runByRunDirectory->mkdir("psd_mod2");
    runByRunPSDResponseMod2Directory->cd();
    for (int m = 0; m < nPSDSections; ++m) {
      name = Form("hPSDMod2SectionsByRun_%d", m + 1);
      hPSDMod2SectionsByRun.at(m) = (TProfile *) hNTracksByRun->Clone(name);
    }
  }

  Info(__func__, "finished");
}

void DataTreeQA::FillEventInfo() {
  double Ereco = fQAConfig.GetPSDEnergyScaleFactor()*DTEvent->GetPSDEnergy();
  double Emc = DTEvent->GetMCPSDEnergy();
  double B = DTEvent->GetImpactParameter();
  double psiRP = DTEvent->GetRPAngle();
  if (psiRP < 0) psiRP += 2 * TMath::Pi();

  if (fExperiment == "NA61") B *= 10e12;

  Int_t mSelected;
  mSelected = fNGoodTracks;

  hTrackCutsEfficiency->Fill(DTEvent->GetNVertexTracks(), mSelected);

  int nPSDModules = DTEvent->GetNPSDModules();
  for (int nMod = 0; nMod < nPSDModules; nMod++) {
    auto psdModule = DTEvent->GetPSDModule(nMod);
    if (psdModule->GetEnergy() >= 0.) {
      h2PSDModuleEnergy->Fill(psdModule->GetId(), psdModule->GetEnergy());
      p2PSDModulePositionEnergy->Fill(psdModule->GetPositionComponent(0),
                                      psdModule->GetPositionComponent(1),
                                      psdModule->GetEnergy());
    }
  }

  for (uint i = 0; i < gPSDCorr.size(); ++i) {
    auto group1 = gPSDCorr[i].first;
    auto group2 = gPSDCorr[i].second;

    if (group1 && group2) {
      hPSDGroupsCorr[i]->Fill(GetPSDGroupEnergy(group1), GetPSDGroupEnergy(group2));
    }
  }

  for (uint i = 0; i < gPSDGroups.size(); ++i) {
    auto group = gPSDGroups[i];
    if (group) {
      hPSDMultCorr[i]->Fill(mSelected, GetPSDGroupEnergy(group));
    }
  }

  for (Int_t iXyz = 0; iXyz < 3; ++iXyz) {
    Int_t kFittedVertexID = fCutsConfig ? fCutsConfig->GetFittedVertexId() : 1;
    hVertexPos[iXyz]->Fill(DTEvent->GetVertexPositionComponent(iXyz, kFittedVertexID));
    h2VertexPosMult[iXyz]->Fill(DTEvent->GetVertexPositionComponent(iXyz, kFittedVertexID), mSelected);
    hMCVertexPos[iXyz]->Fill(DTEvent->GetMCVertexPositionComponent(iXyz));
    hMatchVertexPos[iXyz]->Fill(DTEvent->GetMCVertexPositionComponent(iXyz),
                                DTEvent->GetVertexPositionComponent(iXyz, kFittedVertexID));
  }

  hVertexChi2->Fill(DTEvent->GetVertexQuality());

  if (!fQAConfig.GetIsSimulation()) {
    DataTreeWFA *wfaBeam = DTEvent->GetWFA(0);
    if (wfaBeam) {
      for (auto hit : wfaBeam->GetTimesWFA()) {
        hWFABeamHits->Fill(hit);
      }
    }

#ifdef DATATREE_SHINE
    if (DTEvent->GetNTriggers() > 1) {
      auto s1 = DTEvent->GetTrigger(EnumTrigger::kS1)->GetSignal();
      auto s2 = DTEvent->GetTrigger(EnumTrigger::kS2)->GetSignal();
      hS1->Fill(s1);
      hS2->Fill(s2);
      hS1S2->Fill(s1, s2);
    }
#endif

    for (int iBPD = 0; iBPD < DTEvent->GetNBPDs(); ++iBPD) {
      for (int iXY = 0; iXY < 2; ++iXY) {
        hBeamPosition[iBPD][iXY]->Fill(DTEvent->GetBPD(iBPD)->GetPositionComponent(iXY));
      }
    }
  }

  hMreco->Fill(mSelected);
  hE->Fill(Ereco);
  hMEcorr->Fill(mSelected, Ereco);
  if (fQAConfig.GetIsSimulation()) {
    hB->Fill(B);
    hPsiRP->Fill(psiRP);
    hMEMCcorr->Fill(mSelected, Emc);
    hMBcorr->Fill(B, mSelected);
    hEEcorr->Fill(Emc, Ereco);
    hEBcorr->Fill(B, Ereco);
  }
}

void DataTreeQA::FillRecoTracks() {

  Int_t nTracks = DTEvent->GetNVertexTracks();

  for (int i = 0; i < nTracks; i++) {

    const auto track = DTEvent->GetVertexTrack(i);

    if (fCuts != nullptr && !fCuts->IsGoodTrack(*track)) continue;

    const double phi{track->GetPhi()};
    const int charge{track->GetCharge()};

    const bool isGoingRight = -TMath::PiOver2() < phi && phi < TMath::PiOver2();
    const bool isGoingLeft = !isGoingRight;
    const bool isRST{charge > 0 ? isGoingRight : isGoingLeft};
    const bool isWST{!isRST};

    Float_t dedxcalibr = 1;
    if (fExperiment == "NA49") dedxcalibr = 0.001;

    hNdf->Fill(track->GetNDF());
    hChi2Ndf->Fill(track->GetChi2() / track->GetNDF());

    for (uint tpc = 0; tpc < sTPC->size(); tpc++) {
      Int_t nHits = (tpc == EnumTPC::kTPCAll) ?
                    track->GetNumberOfHits(EnumTPC::kVTPC1) + track->GetNumberOfHits(EnumTPC::kVTPC2)
                        + track->GetNumberOfHits(EnumTPC::kMTPC) : track->GetNumberOfHits(tpc);
      Int_t nHitsPot = (tpc == EnumTPC::kTPCAll) ?
                       track->GetNumberOfHitsPotential(EnumTPC::kVTPC1)
                           + track->GetNumberOfHitsPotential(EnumTPC::kVTPC2)
                           + track->GetNumberOfHitsPotential(EnumTPC::kMTPC) : track->GetNumberOfHitsPotential(tpc);

      hNHits[tpc]->Fill(nHits);
      if (fExperiment != "CBM") {
        hNHitsPot[tpc]->Fill(nHitsPot);
        hNHitsNHitsPot[tpc]->Fill(nHitsPot, nHits);

        if (nHitsPot > 0) {
          hRatioNHitsNHitsPot[tpc]->Fill(double(nHits)/nHitsPot);
        }

        hTrackdEdx[tpc]->Fill(track->GetCharge() * TMath::Log(track->GetP() * 20), dedxcalibr * track->GetdEdx(tpc));

        if (isRST) hTrackdEdxRST[tpc]->Fill(charge * TMath::Log(track->GetP() * 20), dedxcalibr * track->GetdEdx(tpc));
        if (isWST) hTrackdEdxWST[tpc]->Fill(charge * TMath::Log(track->GetP() * 20), dedxcalibr * track->GetdEdx(tpc));
      }

    }

    for (Int_t iXyz = 0; iXyz < 3; ++iXyz) {
      hTrackDCA[iXyz]->Fill(track->GetDCAComponent(iXyz));
    }

    hTrackDCAXY->Fill(track->GetDCAComponent(0), track->GetDCAComponent(1));

    hCharge->Fill(track->GetCharge());
    hChi2Vertex->Fill(track->GetVtxChi2());

    double mom[kAxes];
    mom[kPT] = track->GetPt();
    mom[kETA] = track->GetEta();
    mom[kPHI] = track->GetPhi();
    mom[kPX] = track->GetPx();
    mom[kPY] = track->GetPy();
    mom[kPZ] = track->GetPz();
    mom[kP] = track->GetP();
    mom[kTX] = mom[kPX] / mom[kPZ];
    mom[kTY] = mom[kPY] / mom[kPZ];
    mom[kQP] = track->GetCharge() / mom[kP];
    mom[kYM] = track->GetRapidity();
    mom[kYM] -= fQAConfig.GetRapidityShift();

    for (int j = 0; j < 4; ++j) {
      const Int_t *axes = gTH2Axes[j];
      hTrackMomentumCorr[j]->Fill(mom[axes[0]], mom[axes[1]]);
    }

    for (Int_t iMom = 0; iMom < kAxes; ++iMom) {
      hTrackMomentum[iMom]->Fill(mom[iMom]);
    }

  }

}

void DataTreeQA::FillMCTracks() {
  Double_t psiRP = DTEvent->GetRPAngle();
  DataTreeMCTrack *mcTrack;

  Int_t nGoodTracks = fNGoodTracks;
  Double_t flowSign, flow;
  Double_t mcMomentum[kAxes];

  Int_t yield[kParticles] = {0};
  std::vector<int> Msim(MCnames.size(), 0);

  for (int iMcTrack = 0; iMcTrack < DTEvent->GetNMCTracks(); ++iMcTrack) {
    mcTrack = DTEvent->GetMCTrack(iMcTrack);
    int motherId = mcTrack->GetMotherId();
    int motherIdIndex;
    if (motherId == -1) {
      Msim[0]++;
      motherIdIndex = 0;
    } else {
      Msim[1]++;
      motherIdIndex = 1;
    }
    mcMomentum[kPT] = mcTrack->GetPt();
    mcMomentum[kETA] = mcTrack->GetEta();
    mcMomentum[kPHI] = mcTrack->GetPhi();
    mcMomentum[kYM] = mcTrack->GetRapidity() - fQAConfig.GetRapidityShift();
    mcMomentum[kPX] = mcTrack->GetPx();
    mcMomentum[kPY] = mcTrack->GetPy();
    mcMomentum[kPZ] = mcTrack->GetPz();
    mcMomentum[kP] = mcTrack->GetP();
    mcMomentum[kTX] = mcMomentum[kPX] / mcMomentum[kPZ];
    mcMomentum[kTY] = mcMomentum[kPY] / mcMomentum[kPZ];
    mcMomentum[kQP] = mcTrack->GetCharge() / mcMomentum[kP];

    Int_t pdg = Nint(mcTrack->GetPdgId());
    
#ifdef DATATREE_CBM 
    if (fAnalyzeDileptons) {
      if (motherId != -1 && abs(pdg) == 11) {
        for (int iMcTrack2 = iMcTrack + 1; iMcTrack2 < DTEvent->GetNMCTracks(); ++iMcTrack2) {
          DataTreeMCTrack *mcTrack2 = DTEvent->GetMCTrack(iMcTrack2);
          if (abs(mcTrack2->GetPdgId()) != 11) continue;
          if (mcTrack2->GetMotherId() != motherId) continue;
          TLorentzVector momSum = mcTrack->GetMomentum() + mcTrack2->GetMomentum();
          DataTreeMCTrack *motherMCTrack = DTEvent->GetMCTrack(motherId);
          if (!motherMCTrack) continue;
          int motherPdg = motherMCTrack->GetPdgId();
          if (hMinv_ee.find(motherPdg) == hMinv_ee.end()) continue; 
          hMinv_ee.at(motherPdg) -> Fill (momSum.M());
        }
      }
    }
#endif
    
    if (fAnalyzeEfficiency) // before "identification" fill pt-eta-phi-mult histogram
    {
      Double_t mcEfficiencyBin[4] = {mcMomentum[kPT], mcMomentum[kETA], mcMomentum[kPHI], Double_t(nGoodTracks)};
      if (mcTrack->GetCharge() > 0) hMCTrackPtEtaPhiMultPositive->Fill(mcEfficiencyBin);
      else if (mcTrack->GetCharge() < 0) hMCTrackPtEtaPhiMultNegative->Fill(mcEfficiencyBin);
    }

    for (int j = 0; j < 4; ++j) {
      const Int_t *axes = gTH2Axes[j];
      hMCTrackMomentumCorr[j][motherIdIndex]->Fill(mcMomentum[axes[0]], mcMomentum[axes[1]]);
    }

    for (Int_t iPart = 0; iPart < kParticles; ++iPart) {
      if (gParticles[iPart].pdg == pdg) {
        yield[iPart] += 1;

//                mcMomentum[kYM] = GetRealRapidity(*mcTrack, gParticles[i].mass) - fQAConfig.GetBeamRapidity();
        Double_t mcEfficiencyBin[4] = {mcMomentum[kPT], mcMomentum[kYM], mcMomentum[kPHI], Double_t(nGoodTracks)};

        hMCTrackPtY[iPart][motherIdIndex]->Fill(mcMomentum[kYM], mcMomentum[kPT]);

        if (fAnalyzeEfficiency) hMCTrackPtYPhiMult[iPart]->Fill(mcEfficiencyBin);
        if (motherId == -1) {
          for (Int_t iHarm = 0; iHarm < 2; ++iHarm) {
            if (iHarm % 2 == 0 && mcMomentum[kYM] < 0.) flowSign = -1.;
            else flowSign = 1.;
            flow = cos((iHarm + 1) * (mcMomentum[kPHI] - psiRP));
            pVn_pT[iHarm][iPart]->Fill(mcMomentum[kPT], flowSign * flow);
            pVn_Y[iHarm][iPart]->Fill(mcMomentum[kYM], flow);
          }
        }
      }
    }

  }

  for (Int_t i = 0; i < kParticles; i++) {
    hYields[i]->Fill(DTEvent->GetImpactParameter() * (fExperiment == "NA61" ? 1e13 : 1.0), yield[i]);
  }

  for (uint motherId = 0; motherId < MCnames.size(); motherId++) {
    hMsim[motherId]->Fill(Msim[motherId]);
    hMMcorr[motherId]->Fill(Msim[motherId], nGoodTracks);
  }

}

void DataTreeQA::FillPSDModByMod() {
  int mSelected = fNGoodTracks;
  int nPSDModules = DTEvent->GetNPSDModules();

  for (int im = 0; im < nPSDModules; ++im) {
    h2PSDvsMultiplicity[im]->Fill(mSelected, DTEvent->GetPSDModule(im)->GetEnergy());
    h2PSDRatioVsMultiplicity[im]->Fill(mSelected, DTEvent->GetPSDModule(im)->GetEnergy() / DTEvent->GetPSDEnergy());
  }

//  for (const auto &entry : h2PSDvsMultiplicityLongitudinal) {
//    auto m = DTEvent->GetPSDModule(entry.first);
//
//    {
//      double e_1_5 = 0.0;
//      for (int is = 0; is < 5; ++is) {
//        e_1_5 += m->GetSection(is)->GetEnergy();
//      }
//      entry.second.first->Fill(mSelected, e_1_5);
//    }
//
//    {
//      double e_6_10 = 0.0;
//      for (int is = 5; is < 10; ++is) {
//        e_6_10 += m->GetSection(is)->GetEnergy();
//      }
//      entry.second.second->Fill(mSelected, e_6_10);
//    }
//  }
}

void DataTreeQA::FillTof() {
  for (int i = 0; i < DTEvent->GetNTOFHits(); i++) {
    DataTreeTOFHit *hit = DTEvent->GetTOFHit(i);
    DataTreeTrack *track;
    if (!fCuts->IsGoodTofHit(*hit, *DTEvent)) continue;
    vector <int> recoTrackIds = hit->GetRecoTrackId();
    if (recoTrackIds.empty() || recoTrackIds.at(0) < 0)
      continue;
    if (recoTrackIds.at(0) >= DTEvent->GetNTracks())
      track = DTEvent->GetTrack(recoTrackIds.at(0));
    else
      track = DTEvent->GetVertexTrack(recoTrackIds.at(0));
    if (!fCuts->IsGoodTrack(*track)) continue;
    int mcTrackId = track->GetMCTrackId();
    int mcPid = 0;
    if (mcTrackId > 0 && mcTrackId < DTEvent->GetNMCTracks()) 
      mcPid = DTEvent->GetMCTrack(mcTrackId)->GetPdgId();
    
    float l = track->GetLength();
    float p = track->GetP();
    float time = hit->GetTime();
    int q = track->GetCharge();
    float hitX = hit->GetX();
    float hitY = hit->GetY();
    float hitZ = hit->GetZ();
    float beta = l / time / gSpeedOfLight;
    float m2 = p * p * (1. / beta / beta - 1.);
    
    #ifdef DATATREE_CBM
      DataTreeTrackParams par=track->GetParams(EnumParamsPoint::kTof);
      float trackX=par.GetX();
      float trackY=par.GetY();
      float matchRadius=sqrt(pow(hitX-trackX,2) + pow(hitY-trackY,2));
      hTofMatchRadius->Fill(matchRadius);
      p2TofMatchRadiusXY->Fill(hitX, hitY, matchRadius);
      pTofMatchRadiusZ->Fill(hitZ, matchRadius);
    #endif // DATATREE_CBM
    
    hTofLength->Fill(l);
    hTofTime->Fill(time);
    hTofMatchCharge->Fill(track->GetCharge() * hit->GetCharge());
    hTofZ->Fill(hitZ);
    h2TofXY->Fill(hitX, hitY);
    h2TofM2->Fill(p * q, m2);
    h2TofBeta->Fill(p * q, beta);
    if (h2TofM2Part.find(mcPid) != h2TofM2Part.end())
      h2TofM2Part.at(mcPid)->Fill(p * q, m2);
  }
}

void DataTreeQA::FillV0(bool isReco) {
  int nCandidates = (isReco) ? DTEvent->GetNV0CandidatesTOFpid() : DTEvent->GetNV0CandidatesMCpid();
  for (int iCand = 0; iCand < nCandidates; iCand++) {
    auto cand = (isReco) ? DTEvent->GetV0CandidateTOFpid(iCand) : DTEvent->GetV0CandidateMCpid(iCand);
    int pdg = cand->GetPdgId();
    float mass = cand->GetMomentum().M();
    float pt = cand->GetMomentum().Pt();
    float rap = cand->GetMomentum().Rapidity();
    if (isReco) {
      hV0candMinvTOF.at(pdg)->Fill(mass);
      h2V0candMinvPtTOF.at(pdg)->Fill(pt, mass);
      h2V0candMinvRapTOF.at(pdg)->Fill(rap, mass);
    } else {
      hV0candMinvMC[pdg]->Fill(mass);
      h2V0candMinvPtMC.at(pdg)->Fill(pt, mass);
      h2V0candMinvRapMC.at(pdg)->Fill(rap, mass);
    }
  }
}

void DataTreeQA::Fill_Histograms() {
  if (!DTEvent) return;
  if (!fQAConfig.GetIsSimulation() && fAnalyzeRunByRun) FillRunByRun();
  if (fCuts != nullptr && !fCuts->IsGoodEvent(*DTEvent)) return;

  fNGoodEvents++;

  fNGoodTracks = GetNGoodTracks(*DTEvent, *fCuts);

  FillEventInfo();
  FillRecoTracks();
  FillTof();
  FillV0(true);
  FillV0(false);
  if (fAnalyzePSDModByMod) FillPSDModByMod();
  if (fQAConfig.GetIsSimulation()) {
    FindBestMatches();
    FillMCTracks();
    FillEfficiency();
  }
}

TChain* DataTreeQA::MakeChain(TString filename, TString treename) {
  std::cout << "Adding files to chain:" << std::endl;
  TChain* chain(new TChain(treename));
  if (filename.EndsWith(".root")) {
    chain->Add(filename);
  }
  else {
    TFileCollection fc("fc", "", filename);
    chain->AddFileInfoList(reinterpret_cast<TCollection *>(fc.GetList()));
  }
  chain->ls();
  return chain;
}

void DataTreeQA::Write_Histograms(const TString &filename) {
  MyFile = new TFile(filename, "RECREATE");
  if (MyFile->IsOpen()) Info(__func__, "File '%s' is opened successfully", filename.Data());

  fQAConfig.Clone("config")->Write();
  TDirectory *outputDir = gDirectory;
  TDirectory *currentDir;

  TDirectory *rec = outputDir->mkdir("reco_info");
  if (!fQAConfig.GetIsSimulation() && fAnalyzeRunByRun) {
    TDirectory *rbr = outputDir->mkdir("run_by_run");
    rbr->cd();
    runByRunDirectory->GetList()->Write();
    TDirectory *rbr_mod2 = rbr->mkdir("psd_mod2");
    rbr_mod2->cd();
    runByRunPSDResponseMod2Directory->GetList()->Write();
  }
  rec->cd();

  hVertexChi2->Write();
  for (auto &h : hVertexPos) h->Write();
  for (auto &h2 : h2VertexPosMult) h2->Write();

  hMreco->Write();
  hE->Write();
  hMEcorr->Write();
  hNdf->Write();
  hCharge->Write();
  hChi2Vertex->Write();
  hChi2Ndf->Write();

  h2PSDModuleEnergy->Write();
  p2PSDModulePositionEnergy->Write();

  for (auto psdCorr : hPSDGroupsCorr) {
    if (psdCorr) psdCorr->Write();
  }

  for (auto psdCorr : hPSDMultCorr) {
    if (psdCorr) psdCorr->Write();
  }

  for (uint i = 0; i < sTPC->size(); i++) {
    hNHits[i]->Write();
    if (fExperiment != "CBM") {
      hNHitsPot[i]->Write();
      hNHitsNHitsPot[i]->Write();
      hRatioNHitsNHitsPot[i]->Write();
      hTrackdEdx[i]->Write();
      hTrackdEdxRST[i]->Write();
      hTrackdEdxWST[i]->Write();
    }
  }
  hTofLength->Write();
  hTofTime->Write();
  hTofZ->Write();
  h2TofXY->Write();
  hTofMatchCharge->Write();
  hTofMatchRadius->Write();
  p2TofMatchRadiusXY->Write();
  pTofMatchRadiusZ->Write();
  h2TofBeta->Write();
  h2TofM2->Write();
  for (auto map : hV0candMinvTOF) map.second->Write();
  for (auto map : h2V0candMinvPtTOF) map.second->Write();
  for (auto map : h2V0candMinvRapTOF) map.second->Write();

  if (!fQAConfig.GetIsSimulation() && fExperiment == "NA61") {
    for (int iBPD = 0; iBPD < 3; ++iBPD) {
      for (int iXY = 0; iXY < 2; ++iXY) {
        hBeamPosition[iBPD][iXY]->Write();
      }
    }
    hWFABeamHits->Write();
    hS1->Write();
    hS2->Write();
    hS1S2->Write();
  }
  for (auto &iMom : hTrackMomentum) {
    iMom->Write();
  }

  for (auto &iCorr : hTrackMomentumCorr) {
    iCorr->Write();
  }
  for (int iPart = 0; iPart < 6; iPart++) hTrackPtY[iPart]->Write();

  for (auto &hTrackDCAComponent : hTrackDCA) hTrackDCAComponent->Write();
  hTrackDCAXY->Write();

  hTrackCutsEfficiency->Write();

  if (fQAConfig.GetIsSimulation()) {

    TDirectory *sim = outputDir->mkdir("sim_info");
    TDirectory *recVsSim = outputDir->mkdir("reco_vs_sim_info");
    TDirectory *efficiency = nullptr;
    if (fAnalyzeEfficiency) efficiency = outputDir->mkdir("efficiency");

    sim->cd();

    for (auto &hist : hMCVertexPos) hist->Write();
    hB->Write();
    hPsiRP->Write();
    for (uint motherId = 0; motherId < MCnames.size(); motherId++) {
      hMsim[motherId]->Write();
      for (auto &h2Momentum : hMCTrackMomentumCorr) h2Momentum[motherId]->Write();
      for (auto &hist : hMCTrackPtY) hist[motherId]->Write();
    }
    for (auto map : hV0candMinvMC) map.second->Write();
    for (auto map : h2V0candMinvPtMC) map.second->Write();
    for (auto map : h2V0candMinvRapMC) map.second->Write();
    for (auto &hYield : hYields) hYield->Write();

    for (Int_t iGarm = 0; iGarm < 2; ++iGarm)
      for (Int_t iPart = 0; iPart < kParticles; ++iPart) {
        pVn_pT[iGarm][iPart]->Write();
        pVn_Y[iGarm][iPart]->Write();
      }
        
    if (fAnalyzeDileptons)
      for (auto map : hMinv_ee) map.second->Write();

    recVsSim->cd();
    for (auto &hMatchVertexPo : hMatchVertexPos) hMatchVertexPo->Write();

    hMBcorr->Write();
    hEBcorr->Write();
    for (auto &hist:hMMcorr) hist->Write();
    hEEcorr->Write();
    hMEMCcorr->Write();
    for (auto map : h2TofM2Part) 
      map.second->Write();
    if (fAnalyzeTrackMatching) {
      for (Int_t iMom = 0; iMom < kAxes; ++iMom) {
        currentDir = recVsSim->mkdir(gMomentumAxes[iMom].name + "_matching");
        currentDir->cd();
        hTrackMomentumMatch[iMom]->Write();
        h2TrackResolution[iMom]->Write();
        hTrackResolution[iMom]->Write();
        pTrackResolution[iMom]->Write();

        hTrackMomentumMatchNegative[iMom]->Write();
        h2TrackResolutionNegative[iMom]->Write();
        hTrackResolutionNegative[iMom]->Write();
        pTrackResolutionNegative[iMom]->Write();

        hTrackMomentumMatchPositive[iMom]->Write();
        h2TrackResolutionPositive[iMom]->Write();
        hTrackResolutionPositive[iMom]->Write();
        pTrackResolutionPositive[iMom]->Write();
      }
    }
    recVsSim->cd();

    hTrackMatchCharge->Write();
    pMatchingEfficiency->Write();

    if (fAnalyzeEfficiency) {
      efficiency->cd();
      hTrackPtEtaPhiMultPositive->Write();
      hTrackPtEtaPhiMultNegative->Write();
      hMCTrackPtEtaPhiMultPositive->Write();
      hMCTrackPtEtaPhiMultNegative->Write();
      for (Int_t iPart = 0; iPart < kParticles; ++iPart) {
        hTrackPtYPhiMult[iPart]->Write();
        hMCTrackPtYPhiMult[iPart]->Write();
      }
    }
  }

//  if (fAnalyzePSDModByMod) {
//    auto psdFileDirectory = MyFile->mkdir("psd");
//    psdFileDirectory->cd();
//    psdDirectory->GetList()->Write();
//  }

  MyFile->Close();
}

void DataTreeQA::FillEfficiency() {

  for (auto const &bestMatchEntry : fBestMatches) {
    DataTreeTrackMatch *bestMatch = bestMatchEntry.second;

    DataTreeTrack *recTrack = bestMatch->GetRecTrack();
    DataTreeMCTrack *simTrack = bestMatch->GetMCTrack();

    Double_t recMomentum[kAxes];
    recMomentum[kPT] = recTrack->GetPt();
    recMomentum[kETA] = recTrack->GetEta();
    recMomentum[kPHI] = recTrack->GetPhi();
    recMomentum[kPX] = recTrack->GetPx();
    recMomentum[kPY] = recTrack->GetPy();
    recMomentum[kPZ] = recTrack->GetPz();
    recMomentum[kP] = recTrack->GetP();
    recMomentum[kTX] = recMomentum[kPX] / recMomentum[kPZ];
    recMomentum[kTY] = recMomentum[kPY] / recMomentum[kPZ];
    recMomentum[kQP] = recTrack->GetCharge() / recMomentum[kP];

    Double_t simMomentum[kAxes];
    simMomentum[kPT] = simTrack->GetPt();
    simMomentum[kETA] = simTrack->GetEta();
    simMomentum[kPHI] = simTrack->GetPhi();
    recMomentum[kYM] = recTrack->GetRapidity();
    simMomentum[kPX] = simTrack->GetPx();
    simMomentum[kPY] = simTrack->GetPy();
    simMomentum[kPZ] = simTrack->GetPz();
    simMomentum[kP] = simTrack->GetP();
    simMomentum[kTX] = simMomentum[kPX] / simMomentum[kPZ];
    simMomentum[kTY] = simMomentum[kPY] / simMomentum[kPZ];
    simMomentum[kQP] = simTrack->GetCharge() / simMomentum[kP];

    Double_t recEfficiencyBin[] = {recMomentum[kPT], recMomentum[kETA], recMomentum[kPHI], Double_t(fNGoodTracks)};
    if (fAnalyzeEfficiency) {
      if (recTrack->GetCharge() > 0) hTrackPtEtaPhiMultPositive->Fill(recEfficiencyBin);
      else if (recTrack->GetCharge() < 0) hTrackPtEtaPhiMultNegative->Fill(recEfficiencyBin);
    }
    Int_t pdg = Nint(simTrack->GetPdgId());
    auto particle = GetParticle(pdg);
    Double_t mass;
    if (particle != nullptr) {
      mass = particle->mass;
    } else if (pdg / 1000000000 != 0) {
      mass = abs(pdg % 10000000 / 10000) * 0.931;
    } else mass = 0.;

    recMomentum[kYM] = GetRealRapidity(*recTrack, mass) - fQAConfig.GetRapidityShift();
    simMomentum[kYM] = GetRealRapidity(*simTrack, mass) - fQAConfig.GetRapidityShift();

    if (particle != nullptr) {
      hTrackPtY[particle->id]->Fill(recMomentum[kYM], recMomentum[kPT]);
      if (fAnalyzeEfficiency) {
        Double_t recParticleEfficiencyBin[] =
            {recMomentum[kPT], recMomentum[kYM], recMomentum[kPHI], Double_t(fNGoodTracks)};
        hTrackPtYPhiMult[particle->id]->Fill(recParticleEfficiencyBin);
      }
    }
    
    if (fAnalyzeTrackMatching) {
      for (int j = 0; j < kAxes; ++j) {
        hTrackMomentumMatch[j]->Fill(recMomentum[j], simMomentum[j]);

        Double_t resolution;
        resolution = recMomentum[j] - simMomentum[j];
        if (j == kPT || j == kPX || j == kPY || j == kPZ || j == kP) resolution /= recMomentum[j];

        h2TrackResolution[j]->Fill(recMomentum[j], resolution);
        hTrackResolution[j]->Fill(resolution);
        pTrackResolution[j]->Fill(recMomentum[j], Abs(resolution));

        if (recTrack->GetCharge() == simTrack->GetCharge() && recTrack->GetCharge() == 1) {
          pTrackResolutionPositive[j]->Fill(recMomentum[j], Abs(resolution));
          h2TrackResolutionPositive[j]->Fill(recMomentum[j], resolution);
          hTrackResolutionPositive[j]->Fill(resolution);
          hTrackMomentumMatchPositive[j]->Fill(recMomentum[j], simMomentum[j]);
        }
        if (recTrack->GetCharge() == simTrack->GetCharge() && recTrack->GetCharge() == -1) {
          pTrackResolutionNegative[j]->Fill(recMomentum[j], Abs(resolution));
          h2TrackResolutionNegative[j]->Fill(recMomentum[j], resolution);
          hTrackResolutionNegative[j]->Fill(resolution);
          hTrackMomentumMatchNegative[j]->Fill(recMomentum[j], simMomentum[j]);
        }
      }
    }
    hTrackMatchCharge->Fill(1.0 * recTrack->GetCharge() * simTrack->GetCharge());

  }

}

void DataTreeQA::SetCuts(TObject &fCuts) {
  if (fCuts.IsA() == cuts::DataTreeCutsConfig::Class()) {
    auto &cc = (cuts::DataTreeCutsConfig &) fCuts;
    DataTreeQA::fCuts = new cuts::DataTreeCuts(&cc);
    Info("SetCuts", "Setting cuts from config '%s'", cc.GetName());
    fQAConfig.SetCutsConfig(&cc);
  } else if (fCuts.InheritsFrom(cuts::DataTreeAbstractCuts::Class())) {
    DataTreeQA::fCuts = (cuts::DataTreeAbstractCuts *) &fCuts;
    Info("SetCuts", "Setting cuts from %s", TObject::Class_Name());
  } else {
    Fatal("SetCuts", "Unknown cuts format");
  }
}

void DataTreeQA::SetQAConfig(const DataTreeQAConfig &fQAConfig) {
  DataTreeQA::fQAConfig = fQAConfig;
  fCutsConfig = fQAConfig.GetCutsConfig();
  if (fCutsConfig) {
    SetCuts(*fCutsConfig);
  }
  fExperiment = fQAConfig.GetExperiment();
  fExperiment.ToUpper();
  SetNominalVertexPos(fQAConfig.GetNominalVertex(0), fQAConfig.GetNominalVertex(1), fQAConfig.GetNominalVertex(2));
}

/**
 * Finds best matches (max common points)
 */
void DataTreeQA::FindBestMatches() {

  fBestMatches.clear();

  Int_t nTrackMatches = DTEvent->GetNTrackMatches();

  for (int i = 0; i < nTrackMatches; ++i) {
    DataTreeTrackMatch *match = DTEvent->GetTrackMatch(i);
    DataTreeTrack *recoTrack = match->GetRecTrack();
    DataTreeMCTrack *mcTrack = match->GetMCTrack();

    if (!mcTrack || !recoTrack) continue;
    if (recoTrack->GetCharge() != mcTrack->GetCharge()) continue;

    // select only good tracks
    if (fCuts != nullptr && !fCuts->IsGoodTrack(*recoTrack)) continue;

    auto bestMatchEntry = fBestMatches.find(recoTrack);

    // if current match is better than match in index, then update index
    if (bestMatchEntry != fBestMatches.end()) {
      DataTreeTrackMatch *bestMatch = bestMatchEntry->second;
      if (match->GetNCommonPoints() > bestMatch->GetNCommonPoints()) {
        bestMatchEntry->second = match;
      }
    } else {
      fBestMatches.insert(pair<DataTreeTrack *, DataTreeTrackMatch *>(recoTrack, match));
    }
  }

  pMatchingEfficiency->Fill(fNGoodTracks, 1.0 * fBestMatches.size() / fNGoodTracks);
}

const qa::TParticle *DataTreeQA::GetParticle(Int_t pdgId) {

  for (auto &pp : gParticles) {
    if (pp.pdg == pdgId) return &pp;
  }

  return nullptr;
}

void DataTreeQA::FillRunByRun() {
  const int runId = DTEvent->GetRunId();
  hNEventsInRun->Fill(runId);
  Int_t kFittedVertexID = fCutsConfig ? fCutsConfig->GetFittedVertexId() : 1;

  if (fCuts->IsGoodEvent(*DTEvent)) {
    hNGoodEventsInRun->Fill(runId);
    hNGoodTracksByRun->Fill(runId, GetNGoodTracks(*DTEvent, *fCuts));
    hNTracksByRun->Fill(runId, DTEvent->GetNVertexTracks());
    hPSDEnergyByRun->Fill(runId, DTEvent->GetPSDEnergy());

    for (int iXyz = 0; iXyz < 3; iXyz++) {
      hVertexPosVsRun[iXyz]->Fill(runId, DTEvent->GetVertexPositionComponent(iXyz, kFittedVertexID));
    }

//    for (auto trigger: gTriggerList) {
//      hTriggerByRun[trigger]->Fill(runId, DTEvent->GetTrigger(trigger)->GetSignal());
//    }

    for (int iBPD = 0; iBPD < DTEvent->GetNBPDs(); ++iBPD) {
      for (int iXyz = 0; iXyz < 2; ++iXyz) {

        pBeamPositionVsRun[iBPD][iXyz]->Fill(runId, DTEvent->GetBPD(iBPD)->GetPositionComponent(iXyz));
      }
    }

    int nPSDModules = DTEvent->GetNPSDModules();
    int nPSDSections = DTEvent->GetPSDModule(0)->GetNSections();
    for (int i = 0; i < nPSDModules; ++i) {
      hPSDModuleEnergyByRun[i]->Fill(runId, DTEvent->GetPSDModule(i)->GetEnergy());
    }

    for (int j = 0; j < nPSDSections; ++j) { // what's that?
      hPSDMod2SectionsByRun[j]->Fill(runId, DTEvent->GetPSDModule(1)->GetSection(j)->GetEnergy());
    }
  }
}
