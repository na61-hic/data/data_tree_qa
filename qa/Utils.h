#include <utility>

//
// Created by eugene on 4/9/19.
//

#ifndef DATATREEQA_UTILS_H
#define DATATREEQA_UTILS_H

#include <map>

#include <TString.h>
namespace qa {

class PSDGroupDefinition {
 public:
  unsigned long id;
  TString name;
  TString displayName;
  std::vector<int> modules;

  TString short_name() const {
    TString result;
    Ssiz_t from = 0;

    std::vector<TString> tokens;
    while(name.Tokenize(result, from, "[ /]")) tokens.push_back(result);
    return tokens.back();
  }

  PSDGroupDefinition(unsigned long id, TString name, TString displayName, std::vector<int> modules)
      : id(id), name(std::move(name)), displayName(std::move(displayName)), modules(std::move(modules)) {}
};

// FIXME
// multiple definition of `qa::operator+(...
//PSDGroupDefinition& operator + (PSDGroupDefinition lhs, const PSDGroupDefinition& rhs) {
//  lhs.modules.insert(lhs.modules.end(), rhs.modules.begin(), rhs.modules.end());
//  return lhs;
//}

/**
 * This is potentially will be moved to QA configuration and to
 * PopulateQAConfigurations.C macro
 * But not now...
 */
class PSDGroupsMgr {

 public:

  PSDGroupsMgr() {
    /* NA61 definitions */
    AddPSDGroup("NA61/PSD1", "PSD1", {45, 6, 7, 10, 11, 1, 2, 3, 4, 5, 8, 9, 12, 13, 14, 15, 16});
    AddPSDGroup("NA61/CENTRAL", "#45 + #6,7,10,11",
                {45, 6, 7, 10, 11});
    AddPSDGroup("NA61/PSD2", "PSD2", {17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28});
    AddPSDGroup("NA61/PSD3", "PSD3", {29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44});
    
    /* NA49 definitions */
    AddPSDGroup("NA49/VCAL", "VCAL", 1, 4);
//    AddPSDGroup("NA49/RCAL1", "RCAL1", 5, 124);
//    AddPSDGroup("NA49/RCAL2", "RCAL2", 125, 244);
    for (int i = 0; i < 10; i++)
      AddPSDGroup(Form ("NA49/RCAL%d", i+1), Form ("RCAL%d", i+1), 5+24*i, 4+24*(i+1));
    

    /* CBM44m definitions */
    AddPSDGroup("CBM/44/PSD1", "PSD1", {19, 20, 25, 26});
    AddPSDGroup("CBM/44/PSD2", "PSD2", {12, 13, 14, 15, 18, 21, 24, 27, 30, 31, 32, 33});
    AddPSDGroup("CBM/44/PSD3", "PSD3", {
        1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 16, 17, 22, 23, 28, 29, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44});

    /* CBM46m definitions */
    AddPSDGroup("CBM/46/PSD1", "PSD1", {1, 2, 3, 4, 5, 6, 7, 8});
    AddPSDGroup("CBM/46/PSD2", "PSD2", {9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24});
    AddPSDGroup("CBM/46/PSD3", "PSD3", {25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46});
  }

  const std::shared_ptr<PSDGroupDefinition> AddPSDGroup(const TString &name,
                                                        const TString &displayName,
                                                        const int firstModule,
                                                        const int lastModule) {
    std::vector <int> modules;
    for (int mod = firstModule; mod <= lastModule; mod++)
      modules.push_back(mod);
    return AddPSDGroup(name, displayName, modules);
  }
  
  const std::shared_ptr<PSDGroupDefinition> AddPSDGroup(const TString &name,
                                                        const TString &displayName,
                                                        const std::vector<int> &modules) {
    if (GetPSDGroup(name)) {
      throw std::logic_error("PSD group of name [" + std::string(name.Data()) + "] exists");
    }
    auto id = groupsById.size();
    auto emplace_result = groupsById.emplace(id, std::make_shared<PSDGroupDefinition>(id, name, displayName, modules));
    auto group_ptr = (*emplace_result.first).second;
    groupsByName.insert({name, group_ptr});
    return group_ptr;
  }

  const std::shared_ptr<PSDGroupDefinition> GetPSDGroup(unsigned long id) {
    auto it = groupsById.find(id);
    if (it != groupsById.end()) {
      return (*it).second;
    }

    return {}; // no such definition
  }

  const std::shared_ptr<PSDGroupDefinition> GetPSDGroup(const TString &name) {
    auto it = groupsByName.find(name);
    if (it != groupsByName.end()) {
      return (*it).second;
    }

    return {}; // no such definition
  }
 private:
  std::map<int, std::shared_ptr<PSDGroupDefinition>> groupsById{};
  std::map<TString, std::shared_ptr<PSDGroupDefinition>> groupsByName{};
};

}

#endif //DATATREEQA_UTILS_H
