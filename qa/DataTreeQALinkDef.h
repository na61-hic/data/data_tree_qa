#ifdef __CINT__

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;

#pragma link C++ class DataTreeQAConfig+;

#pragma link C++ namespace qa;
#pragma link C++ class qa::DataTreeQA+;

#pragma link C++ enum qa::EMomentumAxis;
#pragma link C++ struct qa::TMomentumAxis;
#pragma link C++ global qa::gMomentumAxes;

#pragma link C++ enum qa::EParticles;
#pragma link C++ struct qa::TParticle;
#pragma link C++ global qa::gParticles;

//#pragma link C++ enum qa::EPSDGroups;
//#pragma link C++ struct qa::TPSDGroup;
#endif
