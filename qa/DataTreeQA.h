#ifndef DataTreeQA_H
#define DataTreeQA_H 1

#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <map>
#include <unordered_map>

#include <Rtypes.h>
#include <TH3D.h>
#include <THnSparse.h>
#include "TMath.h"
#include "TLorentzVector.h"
#include <TString.h>
#include "TH1.h"
#include "TH2.h"
#include "TFile.h"
#include "TProfile.h"
#include "TProfile2D.h"
#include "TChain.h"

#include <DataTreeEvent.h>
#include <DataTreeTrack.h>
#include "DataTreeQAConfig.h"
#include <cuts/DataTreeAbstractCuts.h>

#include "Utils.h"

namespace qa {
/*
 * Speed of light for TOF
 */
const double gSpeedOfLight = 29.9792458;

/**
 * Axes definition
 */
enum EMomentumAxis {
  kPT = 0, kETA, kPHI, kYM, kPX, kPY, kPZ, kP, kTX, kTY, kQP, kAxes
};

const struct TMomentumAxis {
  Int_t id;
  TString name;
  TString displayName;
  Int_t nBins;
  Double_t min;
  Double_t max;
} gMomentumAxes[kAxes] = {
    /* traverse momentum */
    {.id = kPT, .name = "Pt", .displayName="p_{T} (GeV/#it{c})", .nBins = 1000, .min=0.0, .max=2.5},
    /* pseudorapidity */
    {.id = kETA, .name = "Eta", .displayName="#eta", .nBins = 1000, .min=-4.0, .max=8.0},
    /* azimuthal angle */
    {.id = kPHI, .name = "Phi", .displayName="#varphi (rad)", .nBins = 1000, .min=-TMath::Pi(), .max=TMath::Pi()},
    /* rapidity in CoM frame */
    {.id = kYM, .name = "Y", .displayName="#it{y}", .nBins = 1000, .min=-4.0, .max=4.0},
    /* Px */
    {.id = kPX, .name = "Px", .displayName="P_{x} (GeV/#it{c})", .nBins = 1000, .min=-4.0, .max=4.0},
    /* Py */
    {.id = kPY, .name = "Py", .displayName="P_{y} (GeV/#it{c})", .nBins = 1000, .min=-4.0, .max=4.0},
    /* Pz */
    {.id = kPZ, .name = "Pz", .displayName="P_{z} (GeV/#it{c})", .nBins = 1000, .min=0., .max=15.0},
    /* P */
    {.id = kP, .name = "P", .displayName="P (GeV/#it{c})", .nBins = 1000, .min=0., .max=15.0},
    /* tx = px/pz */
    {.id = kTX, .name = "Tx", .displayName="t_{x} (GeV/#it{c})", .nBins = 1000, .min=-1.0, .max=1.0},
    /* tx = py/pz */
    {.id = kTY, .name = "Ty", .displayName="t_{y} (GeV/#it{c})", .nBins = 1000, .min=-1.0, .max=1.0},
    /* q/p */
    {.id = kQP, .name = "QP", .displayName="q/p (#it{c}/GeV)", .nBins = 1000, .min=-10.0, .max=10.0},
};

/**
 * Particles definition
 */
enum EParticles {
  kPROTON = 0, kPROTONBAR, kPIPLUS, kPIMINUS, kKPLUS, kKMINUS, kDEUTRON, kHE3, kLAMBDA, kLAMBDABAR, kK0s, kParticles
};

const struct TDilepton {
  int pdg;
  std::string name;
  std::string title;
} gDileptons [4] {
      {111, "pi0", "#pi^{0}"}, 
      //{113, "rho0", "#rho^{0}"}, 
      {221, "eta", "#eta"}, 
      {223, "omega", "#omega"},
      {333, "phi", "#phi"}
};

const struct TParticle {
  Int_t id;
  Int_t pdg;
  Double_t mass;
  Int_t charge;
  TString name;
  TString displayName;
} gParticles[kParticles]{
    {kPROTON, 2212, 0.938, 1, "p", "p"},
    {kPROTONBAR, -2212, 0.938, -1, "pbar", "#bar{p}"},
    {kPIPLUS, 211, 0.138, 1, "piplus", "#pi^{+}"},
    {kPIMINUS, -211, 0.138, -1, "piminus", "#pi^{-}"},
    {kKPLUS, 321, 0.494, 1, "kplus", "K^{+}"},
    {kKMINUS, -321, 0.494, -1, "kminus", "K^{-}"},
    {kDEUTRON, 1000010020, 1.876, 1, "d", "_{1}^{2}H"},
    {kHE3, 1000020030, 2.793, 1, "He3", "_{2}^{3}He"},
    {kLAMBDA, 3122, 1.116, 1, "lambda", "#Lambda"},
    {kLAMBDABAR, -3122, 1.116, -1, "lambda_bar", "#bar{#Lambda}"},
    {kK0s, 310, 0.496, 0, "K0s", "K^{0}_{s}"}
};

class DataTreeQA {

 public:
  using PSDGroupDefinitionPtr = std::shared_ptr<PSDGroupDefinition>;


  void Init(TChain *chain);

  /**
   * Definitions of PSD subevent presets
   */
  void Init_Subevents() {
    Info(__func__, "Subevents layout: %s", fPSDSubeventLayoutName.Data());

    qa::PSDGroupsMgr mgr;
    if (fPSDSubeventLayoutName == "NA61_default") {
      kPSD1 = mgr.GetPSDGroup("NA61/PSD1");
      kPSD2 = mgr.GetPSDGroup("NA61/PSD2");
      kPSD3 = mgr.GetPSDGroup("NA61/PSD3");
      kPSDCENTR = mgr.GetPSDGroup("NA61/CENTRAL");
    } else if (fPSDSubeventLayoutName == "NA49_default") {
      kPSD1 = mgr.GetPSDGroup("NA49/VCAL");
      kPSD2 = mgr.GetPSDGroup("NA49/RCAL1");
      kPSD3 = mgr.GetPSDGroup("NA49/RCAL2");
      kPSD4 = mgr.GetPSDGroup("NA49/RCAL3");
      kPSD5 = mgr.GetPSDGroup("NA49/RCAL4");
      kPSD6 = mgr.GetPSDGroup("NA49/RCAL5");
      kPSD7 = mgr.GetPSDGroup("NA49/RCAL6");
      kPSD8 = mgr.GetPSDGroup("NA49/RCAL7");
      kPSD9 = mgr.GetPSDGroup("NA49/RCAL8");
      kPSD10 = mgr.GetPSDGroup("NA49/RCAL9");
      kPSD11 = mgr.GetPSDGroup("NA49/RCAL10");
      
    } else if (fPSDSubeventLayoutName == "CBM44_default") {
      kPSD1 = mgr.GetPSDGroup("CBM/44/PSD1");
      kPSD2 = mgr.GetPSDGroup("CBM/44/PSD2");
      kPSD3 = mgr.GetPSDGroup("CBM/44/PSD3");
    } else if (fPSDSubeventLayoutName == "CBM46_default") {
      kPSD1 = mgr.GetPSDGroup("CBM/46/PSD1");
      kPSD2 = mgr.GetPSDGroup("CBM/46/PSD2");
      kPSD3 = mgr.GetPSDGroup("CBM/46/PSD3");
    } else {
      throw std::logic_error("subevents layout unknown");
    }

//    kPSD1_2 = std::make_shared<PSDGroupDefinition>((*kPSD1) + (*kPSD2));

    gPSDGroups.insert(gPSDGroups.end(), {kPSD1, kPSD2, kPSD3, kPSDCENTR, kPSD1_2});
    gPSDCorr.insert(gPSDCorr.end(), {
        {kPSD1, kPSD2},
        {kPSD2, kPSD3},
        {kPSD1, kPSD3}
    });
//    gPSDGroups.insert(gPSDGroups.end(), {kPSD1, kPSD2, kPSD3, kPSD4, kPSD5, kPSD6, kPSD7, kPSD8, kPSD9, kPSD10, kPSD11});
//    gPSDCorr.insert(gPSDCorr.end(), {
//        {kPSD1, kPSD2},
//        {kPSD1, kPSD3},
//        {kPSD1, kPSD4},
//        {kPSD1, kPSD5},
//        {kPSD1, kPSD6},
//        {kPSD1, kPSD7},
//        {kPSD1, kPSD8},
//        {kPSD1, kPSD9},
//        {kPSD1, kPSD10},
//        {kPSD1, kPSD11},
//    });
  }

  void Init_Histograms();

  void FillEventInfo();

  void FillRecoTracks();

  void FillMCTracks();

  void FillTof();

  void FillV0(bool isReco);

  void Fill_Histograms();

  void FillRunByRun();

  void FillPSDModByMod();

  TChain* MakeChain(TString filename, TString treename);

  void Write_Histograms(const TString& filename);

  const DataTreeQAConfig &GetQAConfig() const { return fQAConfig; }

  void SetQAConfig(const DataTreeQAConfig &fQAConfig);

  cuts::DataTreeAbstractCuts *GetCuts() const { return fCuts; }

  void SetCuts(TObject &fCuts);

  void SetNominalVertexPos(const Float_t x, const Float_t y, const Float_t z) {
    fNominalVertexPos[0] = x;
    fNominalVertexPos[1] = y;
    fNominalVertexPos[2] = z;
    std::printf("NominalVertex = (%f, %f, %f)\n", x, y, z);
  }


  void SetPsdSubeventLayoutName(const TString &_PsdSubeventLayoutName) {
    fPSDSubeventLayoutName = _PsdSubeventLayoutName;
    fPSDLayoutIsSet = true;
  }

  void SetReferenceChain(TChain *fReferenceChain);

  void Run(Long64_t nEvents = 1000000000);

  void FillEfficiency();

  void SetAnalyzeEfficiency(bool analyze) { fAnalyzeEfficiency = analyze; }
  void SetAnalyzeTrackMatching(bool analyze) { fAnalyzeTrackMatching = analyze; }
  void SetAnalyzePSDModByMod(bool analyze) { fAnalyzePSDModByMod = analyze; }
  void SetAnalyzeDileptons(bool analyze) { fAnalyzeDileptons = analyze; }

 private:
  void FindBestMatches();


  inline Double_t GetPSDGroupEnergy(const PSDGroupDefinitionPtr &psdGroupDefPtr) {
    double cumulativeEnergy = 0.;

    if (psdGroupDefPtr->modules.empty()) {
      return 0.;
    }

    for (const int modID : psdGroupDefPtr->modules) {
      auto module = DTEvent->GetPSDModule(modID - 1);

      if (module) {
        cumulativeEnergy += module->GetEnergy();
      } else {
        Warning(__func__, "Module %d is not available in DataTree. Consider another layout...", modID);
      }
    }

    return cumulativeEnergy;
  }

  const TParticle *GetParticle(Int_t pdgId);

  const Int_t gTH2Axes[4][2] = {
      {kETA, kPT},
      {kPHI, kPT},
      {kPHI, kETA},
      {kYM, kPT}
  };

  const TMomentumAxis
      gMultiplicity = {.id = kAxes, .name = "Mult", .displayName = "Multiplicity", .nBins = 250, .min = 0, .max = 250};

  DataTreeQAConfig fQAConfig;
  cuts::DataTreeCutsConfig *fCutsConfig;
  TFile *MyFile{nullptr};
  TChain *fChain{nullptr};
  TChain *fReferenceChain{};
  DataTreeEvent *DTEvent{nullptr};


  /**
   * PSD stuff
   */
  TString fPSDSubeventLayoutName{"NA61_default"}; // name of the PSD subevent layout
  bool fPSDLayoutIsSet{false};
  PSDGroupDefinitionPtr kPSD1{};        //!
  PSDGroupDefinitionPtr kPSD2{};        //!
  PSDGroupDefinitionPtr kPSD3{};        //!
  PSDGroupDefinitionPtr kPSD4{};        //!
  PSDGroupDefinitionPtr kPSD5{};        //!
  PSDGroupDefinitionPtr kPSD6{};        //!
  PSDGroupDefinitionPtr kPSD7{};        //!
  PSDGroupDefinitionPtr kPSD8{};        //!
  PSDGroupDefinitionPtr kPSD9{};        //!
  PSDGroupDefinitionPtr kPSD10{};        //!
  PSDGroupDefinitionPtr kPSD11{};        //!
  PSDGroupDefinitionPtr kPSDCENTR{};    //!
  PSDGroupDefinitionPtr kPSD1_2{};      //!
  std::vector<PSDGroupDefinitionPtr> gPSDGroups{}; //!
  std::vector<std::pair<PSDGroupDefinitionPtr, PSDGroupDefinitionPtr>> gPSDCorr{}; //!

  /**
   * TPC List
   * TODO move outside this class
   */
  const EnumTPC::eTPC gTPCList[EnumTPC::nTPCs] = {EnumTPC::kVTPC1, EnumTPC::kVTPC2, EnumTPC::kMTPC,
                                                  EnumTPC::kTPCAll};
  std::vector<TString> sTPC_na61 = {"VTPC1", "VTPC2", "MTPC", "allTPC"};
  std::vector<TString> sTPC_cbm = {"STS"};
  std::vector<TString> MCnames = {"prim", "sec"};
  std::vector<TString> *sTPC;
  TString fExperiment;
  bool fAnalyzeEfficiency{true};
  bool fAnalyzeTrackMatching{true};
  bool fAnalyzePSDModByMod{false};
  bool fAnalyzeRunByRun{false};
  bool fAnalyzeDileptons{false};

  cuts::DataTreeAbstractCuts *fCuts{nullptr};

  Double_t fNominalVertexPos[3];
  std::map<DataTreeTrack *, DataTreeTrackMatch *> fBestMatches;
  Int_t fNGoodTracks;

  Int_t fNGoodEvents;

  TString sXyz[3] = {"X", "Y", "Z"};
  TH2D *h2PSDModuleEnergy{nullptr};
  TProfile2D *p2PSDModulePositionEnergy{nullptr};

  /* PSD correlation histograms */
  std::vector<TH2D *> hPSDGroupsCorr;
  std::vector<TH2D *> hPSDMultCorr;

  std::vector<TH1D *> hVertexPos{3, nullptr};
  std::vector<TH1D *> hMCVertexPos{3, nullptr};
  std::vector<TH2D *> hMatchVertexPos{3, nullptr};
  std::vector<TH2D *> h2VertexPosMult{3, nullptr};
  TH1D *hVertexChi2{nullptr};

  std::vector<std::vector<TH1D *> > hBeamPosition{3, std::vector<TH1D *>(2, nullptr)};

  TH1F *hPsiRP{nullptr};
  TH1F *hB{nullptr};
  TH1D *hMreco{nullptr};
  TH1D *hE{nullptr};
  TH2D *hMEcorr{nullptr};
  TH2D *hMEMCcorr{nullptr};
  std::vector<TH2D *> hMMcorr{2, nullptr};
  std::vector<TH1D *> hMsim{2, nullptr};
  TH2D *hMBcorr{nullptr};
  TH2D *hEBcorr{nullptr};
  TH2D *hEEcorr{nullptr};
  std::map <int, TH1F *> hMinv_ee;

  //Tracks parameters
  TH1D *hNdf{nullptr};
  TH1D *hCharge{nullptr};
  TH1D *hChi2Vertex{nullptr};
  TH1D *hChi2Ndf{nullptr};

  std::vector<TH1D *> hNHits{EnumTPC::nTPCs, nullptr};
  std::vector<TH1D *> hNHitsPot{EnumTPC::nTPCs, nullptr};
  std::vector<TH1D *> hRatioNHitsNHitsPot{EnumTPC::nTPCs, nullptr};
  std::vector<TH2D *> hTrackdEdx{EnumTPC::nTPCs, nullptr};
  std::vector<TH2D *> hTrackdEdxRST{EnumTPC::nTPCs, nullptr};
  std::vector<TH2D *> hTrackdEdxWST{EnumTPC::nTPCs, nullptr};
  std::map<int, TH2D *> hNHitsNHitsPot;

  std::vector<TH1D *> hTrackDCA{3, nullptr};
  TH2D *hTrackDCAXY{nullptr};

  std::vector<TH1D *> hTrackMomentum{kAxes, nullptr};
  std::vector<TH2D *> hTrackMomentumCorr{4, nullptr};
  std::vector<std::vector<TH2D *> > hMCTrackMomentumCorr{4, std::vector<TH2D *>(2)};

  TProfile *pMatchingEfficiency{nullptr};
  TH1D *hTrackMatchCharge{nullptr};

  std::vector<TH2D *> hTrackMomentumMatch{kAxes, nullptr};
  std::vector<TH2D *> hTrackMomentumMatchPositive{kAxes, nullptr};
  std::vector<TH2D *> hTrackMomentumMatchNegative{kAxes, nullptr};

  std::vector<TH2D *> h2TrackResolution{kAxes, nullptr};
  std::vector<TH2D *> h2TrackResolutionPositive{kAxes, nullptr};
  std::vector<TH2D *> h2TrackResolutionNegative{kAxes, nullptr};

  std::vector<TH1D *> hTrackResolution{kAxes, nullptr};
  std::vector<TH1D *> hTrackResolutionPositive{kAxes, nullptr};
  std::vector<TH1D *> hTrackResolutionNegative{kAxes, nullptr};

  std::vector<TProfile *> pTrackResolution{kAxes, nullptr};
  std::vector<TProfile *> pTrackResolutionPositive{kAxes, nullptr};
  std::vector<TProfile *> pTrackResolutionNegative{kAxes, nullptr};

  // histograms for the efficiency analysis
  std::vector<TH2D *> hTrackPtY{kParticles, nullptr};
  std::vector<std::vector<TH2D *> > hMCTrackPtY{kParticles, std::vector<TH2D *>(2, nullptr)};

  THnSparse *hTrackPtEtaPhiMultPositive{nullptr};
  THnSparse *hTrackPtEtaPhiMultNegative{nullptr};
  THnSparse *hMCTrackPtEtaPhiMultPositive{nullptr};
  THnSparse *hMCTrackPtEtaPhiMultNegative{nullptr};

  std::unordered_map<int, THnSparse *> hTrackPtYPhiMult;
  std::unordered_map<int, THnSparse *> hMCTrackPtYPhiMult;

  //TOF
  TH1F *hTofLength{nullptr};
  TH1F *hTofTime{nullptr};
  TH1F *hTofZ{nullptr};
  TH2F *h2TofXY{nullptr};
  TH1I *hTofMatchCharge{nullptr};
  TH1F *hTofMatchRadius{nullptr};
  TProfile2D *p2TofMatchRadiusXY{nullptr};
  TProfile *pTofMatchRadiusZ{nullptr};
  TH2F *h2TofBeta{nullptr};
  TH2F *h2TofM2{nullptr};
  std::map<int, TH2F *> h2TofM2Part;

  // V0
  std::map<int, TH1F *> hV0candMinvTOF;
  std::map<int, TH2F *> h2V0candMinvRapTOF;
  std::map<int, TH2F *> h2V0candMinvPtTOF;
  std::map<int, TH1F *> hV0candMinvMC;
  std::map<int, TH2F *> h2V0candMinvRapMC;
  std::map<int, TH2F *> h2V0candMinvPtMC;

  //WFA
  TH1D *hWFABeamHits{nullptr};

  // Triggers
  TH1D *hS1{nullptr};
  TH1D *hS2{nullptr};
  TH2D *hS1S2{nullptr};

  // Flow
  std::vector<std::vector<TProfile *> > pVn_pT{2, std::vector<TProfile *>(kParticles, nullptr)};
  std::vector<std::vector<TProfile *> > pVn_Y{2, std::vector<TProfile *>(kParticles, nullptr)};

  // Yields
  std::vector<TProfile *> hYields{kParticles, nullptr};

  // Cuts efficiency
  TH2 *hTrackCutsEfficiency{nullptr};

  // Run-by-run histograms
  TDirectory *runByRunDirectory{nullptr};

  TH1I *hNEventsInRun{nullptr};
  TH1I *hNGoodEventsInRun{nullptr};

  TProfile *hNGoodTracksByRun{nullptr};
  TProfile *hNTracksByRun{nullptr};

  TProfile *hPSDEnergyByRun{nullptr};
  std::vector<TProfile *> hPSDModuleEnergyByRun{45, nullptr};

  std::vector<TProfile *> hVertexPosVsRun{3, nullptr};
  std::vector<std::vector<TProfile *> > pBeamPositionVsRun{3, std::vector<TProfile *>(2, nullptr)};

  EnumTrigger::eTriggerId gTriggerList[3] = {EnumTrigger::kS1, EnumTrigger::kS2, EnumTrigger::kS3};
//  std::unordered_map<int, TProfile *> hTriggerByRun;

  TDirectory *runByRunPSDResponseMod2Directory{nullptr};
  std::vector<TProfile *> hPSDMod2SectionsByRun{10, nullptr};

  TDirectory *psdDirectory{nullptr};
  std::vector<TH2D *> h2PSDvsMultiplicity;
  std::vector<TH2D *> h2PSDRatioVsMultiplicity;
//  std::unordered_map<int, std::pair<TH2D *, TH2D *>> h2PSDvsMultiplicityLongitudinal;
};

}
#endif
