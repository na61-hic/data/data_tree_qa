//
// Created by eugene on 2/20/18.
//

#include "DataTreeCuts.h"

ClassImp(cuts::DataTreeCuts)

using namespace cuts;
using namespace TMath;

Bool_t DataTreeCuts::IsGoodEvent(const DataTreeEvent &event) {

  if (fCutsConfig->IsApplyEventQuality()) {
    if (fCutsConfig->GetNonZeroEnergyPSD() && !(event.GetPSDEnergy() > 0)) return kFALSE;

    Int_t nGoodTracks = 0;
    for (int i = 0; i < event.GetNVertexTracks(); ++i) {
      if (IsGoodTrack(*event.GetVertexTrack(i))) nGoodTracks++;
    }

    if (!(nGoodTracks > fCutsConfig->GetNGoodTracksMin())) return kFALSE;

    if (
    		fCutsConfig->GetEvsMBottom() &&
    		fCutsConfig->GetEvsMBottom()->isInitialized() &&
			!fCutsConfig->GetEvsMBottom()->isVertical() &&
			fCutsConfig->GetEvsMBottom()->isBelow(nGoodTracks, event.GetPSDEnergy())
    ) return kFALSE;

    if (fCutsConfig->IsApplyEventBPD() && !IsGoodBPD(event)) return kFALSE;
    if (fCutsConfig->IsApplyEventVertex() && !IsGoodVertex(event)) return kFALSE;
    if (fCutsConfig->IsApplyTriggers() && !IsGoodSTriggers(event)) return kFALSE;
    if (fCutsConfig->IsApplyWFA() && !IsGoodWFA(event)) return kFALSE;

    if (fCutsConfig->IsApplyT2T4())
      if (!(event.GetTrigger(EnumTrigger::kT4)->GetIsFired() || event.GetTrigger(EnumTrigger::kT2)->GetIsFired()))
        return kFALSE;

    if (fCutsConfig->IsApplyT2() && !(event.GetTrigger(EnumTrigger::kT2)->GetIsFired())) return kFALSE;
    if (fCutsConfig->IsApplyT4() && !(event.GetTrigger(EnumTrigger::kT4)->GetIsFired())) return kFALSE;
  }

  return kTRUE;
}
  
Bool_t DataTreeCuts::IsGoodTrack(const DataTreeTrack &track) {

  const Double_t dca_X = track.GetDCAComponent(0);
  const Double_t dca_Y = track.GetDCAComponent(1);
  const Double_t chi2 = track.GetChi2() / track.GetNDF();
  const Double_t chi2Vtx = track.GetVtxChi2();
  const Int_t nHitsVTPC1 = track.GetNumberOfHits(EnumTPC::kVTPC1);
  const Int_t nHitsVTPC2 = track.GetNumberOfHits(EnumTPC::kVTPC2);
  const Int_t nHitsMTPC = track.GetNumberOfHits(EnumTPC::kMTPC);
  const Int_t nHitsPotVTPC1 = track.GetNumberOfHitsPotential(EnumTPC::kVTPC1);
  const Int_t nHitsPotVTPC2 = track.GetNumberOfHitsPotential(EnumTPC::kVTPC2);
  const Int_t nHitsPotMTPC = track.GetNumberOfHitsPotential(EnumTPC::kMTPC);
  const Int_t nHits = nHitsVTPC1 + nHitsVTPC2 + nHitsMTPC;
  const Int_t nHitsPot = nHitsPotVTPC1 + nHitsPotVTPC2 + nHitsPotMTPC;

  auto& nHitsPotVTPC1orVTPC2orMTPCMin = fCutsConfig->GetNHitsPotVTPC1orVTPC2orMTPCMin();

  if (fCutsConfig->IsApplyTrackQuality()) {

    if (fCutsConfig->IsApplyNumberOfHitsCut()) {
      if (!(nHitsPot != 0 &&
            fCutsConfig->GetNHitsToNHitsPotMin() < (double)nHits / nHitsPot &&
            fCutsConfig->GetNHitsToNHitsPotMax() > (double)nHits / nHitsPot))
        return kFALSE;

      if (!(nHits >= fCutsConfig->GetNHitsTotalMin()))
        return kFALSE;

      if (!(nHitsVTPC1 + nHitsVTPC2 >= fCutsConfig->GetNHitsVTPCMin()))
        return kFALSE;

      if (!(nHitsPotVTPC1 + nHitsPotVTPC2 >= fCutsConfig->GetNHitsPotVTPCMin()))
        return kFALSE;

      if (!(nHitsMTPC >= fCutsConfig->GetNHitsMTPCMin()))
        return kFALSE;

      if (!(nHitsPotMTPC >= fCutsConfig->GetNHitsPotMTPCMin()))
        return kFALSE;

      if (!(nHitsPotMTPC >= nHitsPotVTPC1orVTPC2orMTPCMin.at(0)) &&
          !(nHitsPotVTPC1 >= nHitsPotVTPC1orVTPC2orMTPCMin.at(1)) &&
          !(nHitsPotVTPC2 >= nHitsPotVTPC1orVTPC2orMTPCMin.at(2)))
        return kFALSE;
    }

    if (!(chi2 < fCutsConfig->GetTrackChiMax()))
      return kFALSE;

    if (!(chi2Vtx < fCutsConfig->GetTrackChiVtxMax()))
      return kFALSE;

    if (!(fabs(dca_X - fCutsConfig->GetDCA_X_Center()) < fCutsConfig->GetDCA_X_Max()))
      return kFALSE;

    if (!(fabs(dca_Y - fCutsConfig->GetDCA_Y_Center()) < fCutsConfig->GetDCA_Y_Max()))
      return kFALSE;

    if (!(Sqrt(Power(dca_X - fCutsConfig->GetDCA_X_Center(), 2.0) + Power(dca_Y - fCutsConfig->GetDCA_Y_Center(), 2.0))
        < fCutsConfig->GetDCA_Max()))
      return kFALSE;
  }

  if (fCutsConfig->IsApplyTrackKinematics()) {
    if (!(fCutsConfig->GetPtMin() < track.GetPt() && track.GetPt() < fCutsConfig->GetPtMax())) return kFALSE;
    if (!(fCutsConfig->GetEtaMin() < track.GetEta() && track.GetEta() < fCutsConfig->GetEtaMax())) return kFALSE;
  }

  if (fCutsConfig->IsApplyTrackPzCut()) {
    if (! (fCutsConfig->GetPzMin() < track.GetPz())) return kFALSE;
  }

  return kTRUE;
}

Bool_t DataTreeCuts::IsGoodTofHit(const DataTreeTOFHit &hit, const DataTreeEvent &event) {
  if (!fCutsConfig->IsApplyTofHitQuality()) return true;
  std::vector <int> recoTrackIds = hit.GetRecoTrackId();
  if (recoTrackIds.empty() || recoTrackIds.at(0) < 0 || recoTrackIds.at(0) >= event.GetNTracks())
    return false;
  DataTreeTrack *track = event.GetVertexTrack(recoTrackIds.at(0));
  if (!IsGoodTrack(*track)) return false;
  float hitX = hit.GetX();
  float hitY = hit.GetY();
  float hitZ = hit.GetZ();
  std::vector <float> hitPathLengthLimits = fCutsConfig->GetTofHitPathLengthLimits();
  std::vector <float> hitTimeLimits = fCutsConfig->GetTofHitTimeLimits();
  std::vector <float> hitBetaLimits = fCutsConfig->GetTofHitBetaLimits();
  std::vector <float> hitPositionLimits = fCutsConfig->GetTofHitPositionLimits();
  if (hit.GetCharge() != track->GetCharge()) return false;
  if (!(hit.GetTime() > hitTimeLimits.at(0)) || !(hit.GetTime() < hitTimeLimits[1])) return false;
  if (!(hit.GetBeta() > hitBetaLimits.at(0)) || !(hit.GetBeta() < hitBetaLimits[1])) return false;
  if (!(hit.GetPathLength() > hitPathLengthLimits.at(0)) || !(hit.GetPathLength() < hitPathLengthLimits.at(1))) return false;
  if (!(hitX > hitPositionLimits.at(0)) || !(hitX < hitPositionLimits.at(1)) ||
      !(hitY > hitPositionLimits.at(2)) || !(hitY < hitPositionLimits.at(3)) ||
      !(hitZ > hitPositionLimits.at(4)) || !(hitZ < hitPositionLimits.at(5))) return false;
  
  #ifdef DATATREE_CBM
    DataTreeTrackParams par=track->GetParams(EnumParamsPoint::kTof);
    float trackX=par.GetX();
    float trackY=par.GetY();
    float matchRadius=sqrt(pow(hitX-trackX,2) + pow(hitY-trackY,2));
    if (!(matchRadius < fCutsConfig->GetTofHitMatchRadius())) return false;
  #endif // DATATREE_CBM
  
  return true;
}

Bool_t DataTreeCuts::IsGoodBPD(const DataTreeEvent &event) {

  for (Int_t iBpd = 0; iBpd < 3; iBpd++) {
    for (Int_t iXy = 0; iXy < 2; ++iXy) {
      Double_t bpdPos = event.GetBPD(iBpd)->GetPositionComponent(iXy);
      if (!(fCutsConfig->GetBPDMin(iBpd, iXy) < bpdPos && bpdPos < fCutsConfig->GetBPDMax(iBpd, iXy))) return kFALSE;
    }
  }
  return kTRUE;
}

Bool_t DataTreeCuts::IsGoodWFA(const DataTreeEvent &event) {
  DataTreeWFA *wfa = event.GetWFA(fCutsConfig->GetWFATriggerId());

  Int_t nHits = 0;
  for (auto hitTime : wfa->GetTimesWFA()) {
    if (Abs(hitTime) < fCutsConfig->GetWFATimeWindow()) nHits++;
  }

  if (nHits != 1) return kFALSE;
  return kTRUE;
}

Bool_t DataTreeCuts::IsGoodVertex(const DataTreeEvent &event) {
  double vertexChi2Max = fCutsConfig->GetVertexChi2Max();
  if (vertexChi2Max > 0 &&
      event.GetVertexQuality(fCutsConfig->GetFittedVertexId()) > vertexChi2Max)
    return kFALSE;
  if (TMath::Abs(event.GetVertexPositionComponent(2, fCutsConfig->GetFittedVertexId()) + 591.9000244) < 0.00001)
    return kFALSE;// TODO fix it
  for (Int_t iXyz = 0; iXyz < 3; ++iXyz) {
    Double_t vtxPos = event.GetVertexPositionComponent(iXyz, fCutsConfig->GetFittedVertexId());

    if (!(fCutsConfig->GetVertexMin(iXyz) < vtxPos && vtxPos < fCutsConfig->GetVertexMax(iXyz))) return kFALSE;
  }
  return kTRUE;
}

Bool_t DataTreeCuts::IsGoodSTriggers(const DataTreeEvent &event) {
  Double_t s1 = event.GetTrigger(EnumTrigger::kS1)->GetSignal();
  Double_t s2 = event.GetTrigger(EnumTrigger::kS2)->GetSignal();

  if (fCutsConfig->GetSLinearCutParams(1) * s1 + fCutsConfig->GetSLinearCutParams(2) * s2
      > fCutsConfig->GetSLinearCutParams(0))
    return kFALSE;
  if (!(fCutsConfig->GetS1Min() < s1 && s1 < fCutsConfig->GetS1Max())) return kFALSE;
  if (!(fCutsConfig->GetS2Min() < s2 && s2 < fCutsConfig->GetS2Max())) return kFALSE;

  return kTRUE;
}
