//
// Created by eugene on 2/20/18.
//

#ifndef DATATREE_DataTreeCuts_H
#define DATATREE_DataTreeCuts_H

#include <Rtypes.h>
#include <DataTreeEvent.h>
#include "DataTreeAbstractCuts.h"
#include "DataTreeCutsConfig.h"

namespace cuts {
class DataTreeCuts : public DataTreeAbstractCuts {

 ClassDefOverride(cuts::DataTreeCuts, 0)

 public:
  DataTreeCuts(DataTreeCutsConfig *fCutsConfig) : fCutsConfig(fCutsConfig) {}

  virtual ~DataTreeCuts() {};

  Bool_t IsGoodEvent(const DataTreeEvent &event) override;

  Bool_t IsGoodTrack(const DataTreeTrack &track) override;
  
  Bool_t IsGoodTofHit(const DataTreeTOFHit &hit, const DataTreeEvent &event) override;

  Bool_t IsGoodBPD(const DataTreeEvent &event);

  Bool_t IsGoodVertex(const DataTreeEvent &event);

  Bool_t IsGoodSTriggers(const DataTreeEvent &event);

  Bool_t IsGoodWFA(const DataTreeEvent &event);
 protected:
  DataTreeCutsConfig *fCutsConfig;

 public:
  DataTreeCutsConfig *GetCutsConfig() const {
    return fCutsConfig;
  }

  void SetCutsConfig(DataTreeCutsConfig *fCutsConfig) {
    DataTreeCuts::fCutsConfig = fCutsConfig;
  }

};

}

#endif //DATATREE_DataTreeCuts_H
