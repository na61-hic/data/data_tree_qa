//
// Created by eugene on 2/20/18.
//

#ifndef DATATREE_DATATREEABSTRACTCUT_H
#define DATATREE_DATATREEABSTRACTCUT_H


#include <Rtypes.h>
#include <DataTreeEvent.h>

namespace cuts {


class DataTreeAbstractCuts {
public:
    DataTreeAbstractCuts() {};

    virtual ~DataTreeAbstractCuts() = default;;

    virtual Bool_t IsGoodEvent(const DataTreeEvent&) {
      return true;
    }

    virtual Bool_t IsGoodTrack(const DataTreeTrack&) {
      return true;
    }
    
    virtual Bool_t IsGoodTofHit(const DataTreeTOFHit&, const DataTreeEvent&) {
      return true;
    }


    ClassDef(cuts::DataTreeAbstractCuts,0);

};

}

#endif //DATATREE_DATATREEABSTRACTCUT_H
