set(PROJECT_NAME_CUTS DataTreeCuts)

set(PCM_FILE_NAME_PREFIX lib${PROJECT_NAME_CUTS})
set(PCM_FILE ${CMAKE_CURRENT_BINARY_DIR}/${PCM_FILE_NAME_PREFIX}_rdict.pcm)

if (NOT EXPORTS_DIR)
    message(FATAL_ERROR "EXPORTS_DIR is not set")
endif ()
set(CUTS_EXPORTS_FILE ${EXPORTS_DIR}/datatreecuts_exports.h)

ROOT_GENERATE_DICTIONARY(G__DataTreeCuts
        	DataTreeAbstractCuts.h
        	DataTreeCutsConfig.h
        	DataTreeCuts.h
        LINKDEF
        	DataTreeCutsLinkDef.h
        OPTIONS
        	-I${CMAKE_SOURCE_DIR}
        	-I${DataTree_INCLUDE_DIR}
        	-DDATATREE_${EXPERIMENT}
)

add_library(${PROJECT_NAME_CUTS} SHARED 
	./DataTreeCuts.cxx 
	./DataTreeCutsConfig.cxx
	G__DataTreeCuts.cxx
)
target_link_libraries(${PROJECT_NAME_CUTS}
	PRIVATE 
		# ROOT libraries are nested to DataTree
		DataTree
)
target_include_directories(${PROJECT_NAME_CUTS}
	# @see https://stackoverflow.com/questions/25676277/cmake-target-include-directories-prints-an-error-when-i-try-to-add-the-source
	PUBLIC
		$<BUILD_INTERFACE:${CMAKE_SOURCE_DIR}>
		$<INSTALL_INTERFACE:include>
	PRIVATE
		# to be able to compile G__DataTreeCuts.cxx
		$<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}>
)		

generate_export_header(${PROJECT_NAME_CUTS} EXPORT_FILE_NAME ${CUTS_EXPORTS_FILE})

install(TARGETS ${PROJECT_NAME_CUTS} EXPORT DataTreeQATargets
        LIBRARY DESTINATION lib
        ARCHIVE DESTINATION lib
        RUNTIME DESTINATION bin
        INCLUDES DESTINATION include/cuts
        )
        
install(DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}
	DESTINATION
		include
    FILES_MATCHING PATTERN "*.h")

install(
        FILES
        	${CUTS_EXPORTS_FILE}
        DESTINATION
        	include/cuts
        COMPONENT
        	Devel
)

install(
        FILES
        	${PCM_FILE}
        DESTINATION
        	lib
        OPTIONAL
)
