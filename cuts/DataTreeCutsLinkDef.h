#ifdef __CINT__

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;


#pragma link C++ namespace cuts;
#pragma link C++ class cuts::DataTreeAbstractCuts+;

#pragma link C++ class cuts::Line+;
#pragma link C++ class cuts::DataTreeCuts+;
#pragma link C++ class cuts::DataTreeCutsConfig+;

#endif // __CINT__
