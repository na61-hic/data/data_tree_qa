//
// Created by eugene on 3/26/18.
//

#ifndef DATATREEQA_DATATREECUTSCONFIG_H
#define DATATREEQA_DATATREECUTSCONFIG_H

#include <Rtypes.h>
#include <TNamed.h>
#include <cassert>

namespace cuts {

class Line : public TObject {

public:
	Line() : TObject() {};
	Line (Double_t x1, Double_t y1, Double_t x2, Double_t y2) : TObject(),
	 	fX1(x1), fY1(y1), fX2(x2), fY2(y2), fIsInitialized(true) {
		assert(!(fX1 == fX2 && fY1 == fY2)); /* p1 != p2 */
	}

	~Line() {}

	bool isVertical() const {
		checkInitialized();
		return fX1 == fX2;
	}

	bool isUpper(Double_t x, Double_t y) const {
		return y > this->y(x);
	}

	bool isBelow(Double_t x, Double_t y) const {
		return y < this->y(x);
	}

	bool isInitialized() const {
		return fIsInitialized;
	}

	void checkInitialized() const {
		if (!fIsInitialized) {
			throw std::runtime_error("cuts::Line is not initialized");
		}
	}

	Double_t k() const {
		checkInitialized();
		assert(!isVertical());
		return (fY2 - fY1)/(fX2 - fX1);
	}

	Double_t y(Double_t x) const {
		checkInitialized();
		assert (!isVertical());
		return k() * (x - fX1) + fY1;
	}

private:
	bool fIsInitialized{false};
	Double_t fX1{0.};
	Double_t fY1{0.};
	Double_t fX2{0.};
	Double_t fY2{0.};

	ClassDefNV(cuts::Line, 2);
};



class DataTreeCutsConfig : public TNamed {

 public:
  DataTreeCutsConfig() = default;
  DataTreeCutsConfig(const char *name, const char *title = "") : TNamed(name, title) {}
  DataTreeCutsConfig(const TString &name, const TString &title = "") : TNamed(name, title) {}

  Bool_t IsApplyTrackQuality() const {
    return fApplyTrackQuality;
  }

  void ApplyTrackQuality(Bool_t fApplyTrackQuality) {
    DataTreeCutsConfig::fApplyTrackQuality = fApplyTrackQuality;
  }
  
  Bool_t IsApplyTofHitQuality() const {
    return fApplyTofHitQuality;
  }
  
  void ApplyTofHitQuality(Bool_t fApplyTofHitQuality) {
    DataTreeCutsConfig::fApplyTofHitQuality = fApplyTofHitQuality;
  }

  float GetTofHitMatchRadius() const {
    return fTofHitMatchRadius;
  }

  void SetTofHitMatchRadius(float hitMatchRadius) {
    DataTreeCutsConfig::fTofHitMatchRadius = hitMatchRadius;
  }
  
  std::vector<float> GetTofHitPathLengthLimits() const {
    return fTofHitPathLength;
  }

  void SetTofHitPathLengthLimits(float hitPathLengthMin, float hitPathLengthMax) {
    DataTreeCutsConfig::fTofHitPathLength.at(0) = hitPathLengthMin;
    DataTreeCutsConfig::fTofHitPathLength.at(1) = hitPathLengthMax;
  }
  
  std::vector<float> GetTofHitTimeLimits() const {
    return fTofHitTime;
  }

  void SetTofHitTimeLimits(float hitTimeMin, float hitTimeMax) {
    DataTreeCutsConfig::fTofHitTime.at(0) = hitTimeMin;
    DataTreeCutsConfig::fTofHitTime.at(1) = hitTimeMax;
  }
  
  std::vector<float> GetTofHitBetaLimits() const {
    return fTofHitBeta;
  }

  void SetTofHitBetaLimits(float hitBetaMin, float hitBetaMax) {
    DataTreeCutsConfig::fTofHitBeta.at(0) = hitBetaMin;
    DataTreeCutsConfig::fTofHitBeta.at(1) = hitBetaMax;
  }

  std::vector<float> GetTofHitPositionLimits() const {
    return fTofHitPosition;
  }

  void SetTofHitPositionLimits(float hitXMin=0., float hitXMax=100000., 
                         float hitYMin=0., float hitYMax=100000., 
                         float hitZMin=0., float hitZMax=100000.) {
    DataTreeCutsConfig::fTofHitPosition.at(0) = hitXMin;
    DataTreeCutsConfig::fTofHitPosition.at(1) = hitXMax;
    DataTreeCutsConfig::fTofHitPosition.at(2) = hitYMin;
    DataTreeCutsConfig::fTofHitPosition.at(3) = hitYMax;
    DataTreeCutsConfig::fTofHitPosition.at(4) = hitZMin;
    DataTreeCutsConfig::fTofHitPosition.at(5) = hitZMax;
  }
  
  void ApplyNumberOfHitsCut(Bool_t fApplyNumberOfHitsCut) {
    DataTreeCutsConfig::fApplyNumberOfHitsCut = fApplyNumberOfHitsCut;
  }

  Bool_t IsApplyNumberOfHitsCut() const {
    return fApplyNumberOfHitsCut;
  }

  Int_t GetNHitsVTPCMin() const {
    return fNHitsVTPCMin;
  }

  void SetNHitsVTPCMin(Int_t fNHitsVTPCMin) {
    DataTreeCutsConfig::fNHitsVTPCMin = fNHitsVTPCMin;
  }

  Int_t GetNHitsMTPCMin() const {
    return fNHitsMTPCMin;
  }

  void SetNHitsMTPCMin(Int_t fNHitsMTPCMin) {
    DataTreeCutsConfig::fNHitsMTPCMin = fNHitsMTPCMin;
  }

  Int_t GetNHitsPotVTPCMin() const {
    return fNHitsPotVTPCMin;
  }

  void SetNHitsPotVTPCMin(Int_t fNHitsPotVTPCMin) {
    DataTreeCutsConfig::fNHitsPotVTPCMin = fNHitsPotVTPCMin;
  }
  
  Int_t GetNHitsPotVTPC1Min() const {
    return fNHitsPotVTPC1Min;
  }

  void SetNHitsPotVTPC1Min(Int_t fNHitsPotVTPC1Min) {
    DataTreeCutsConfig::fNHitsPotVTPC1Min = fNHitsPotVTPC1Min;
  }
  
  Int_t GetNHitsPotVTPC2Min() const {
    return fNHitsPotVTPC2Min;
  }

  void SetNHitsPotVTPC2Min(Int_t fNHitsPotVTPC2Min) {
    DataTreeCutsConfig::fNHitsPotVTPC2Min = fNHitsPotVTPC2Min;
  }

  Int_t GetNHitsPotMTPCMin() const {
    return fNHitsPotMTPCMin;
  }

  void SetNHitsPotMTPCMin(Int_t fNHitsPotMTPCMin) {
    DataTreeCutsConfig::fNHitsPotMTPCMin = fNHitsPotMTPCMin;
  }

  Double_t GetTrackChiMax() const {
    return fTrackChiMax;
  }

  void SetTrackChiMax(Double_t fTrackChiMax) {
    DataTreeCutsConfig::fTrackChiMax = fTrackChiMax;
  }

  Double_t GetTrackChiVtxMax() const {
    return fTrackChiVtxMax;
  }
  void SetTrackChiVtxMax(Double_t fTrackChiVtxMax) {
    DataTreeCutsConfig::fTrackChiVtxMax = fTrackChiVtxMax;
  }

  Double_t GetDCA_Max() const {
    return fDCA_Max;
  }

  Double_t GetDCA_X_Max() const {
    return fDCA_X_Max;
  }

  Double_t GetDCA_Y_Max() const {
    return fDCA_Y_Max;
  }

  void SetDCA_Max(Double_t fDCA_Max) {
    DataTreeCutsConfig::fDCA_Max = fDCA_Max;
  }

  void SetDCA_X_Max(Double_t fDCA_X_Max) {
    DataTreeCutsConfig::fDCA_X_Max = fDCA_X_Max;
  }

  void SetDCA_Y_Max(Double_t fDCA_Y_Max) {
    DataTreeCutsConfig::fDCA_Y_Max = fDCA_Y_Max;
  }

  Bool_t IsApplyTrackKinematics() const {
    return fApplyTrackKinematics;
  }

  void ApplyTrackKinematics(Bool_t fApplyTrackKinematics) {
    DataTreeCutsConfig::fApplyTrackKinematics = fApplyTrackKinematics;
  }

  Double_t GetPtMin() const {
    return fPtMin;
  }

  void SetPtMin(Double_t fPtMin) {
    DataTreeCutsConfig::fPtMin = fPtMin;
  }

  Double_t GetPtMax() const {
    return fPtMax;
  }

  void SetPtMax(Double_t fPtMax) {
    DataTreeCutsConfig::fPtMax = fPtMax;
  }

  Double_t GetEtaMin() const {
    return fEtaMin;
  }

  void SetEtaMin(Double_t fEtaMin) {
    DataTreeCutsConfig::fEtaMin = fEtaMin;
  }

  Double_t GetEtaMax() const {
    return fEtaMax;
  }

  void SetEtaMax(Double_t fEtaMax) {
    DataTreeCutsConfig::fEtaMax = fEtaMax;
  }

  Bool_t IsApplyEventQuality() const {
    return fApplyEventQuality;
  }

  void ApplyEventQuality(Bool_t fApplyEventQuality) {
    DataTreeCutsConfig::fApplyEventQuality = fApplyEventQuality;
  }

  Bool_t GetNonZeroEnergyPSD() const {
    return fNonZeroEnergyPSD;
  }
  void NonZeroEnergyPSD(Bool_t fNonZeroEnergyPSD) {
    DataTreeCutsConfig::fNonZeroEnergyPSD = fNonZeroEnergyPSD;
  }

  Int_t GetNGoodTracksMin() const {
    return fNGoodTracksMin;
  }

  void SetNGoodTracksMin(Int_t fNGoodTracksMin) {
    DataTreeCutsConfig::fNGoodTracksMin = fNGoodTracksMin;
  }

  Bool_t IsApplyEventVertex() const {
    return fApplyEventVertex;
  }

  void ApplyEventVertex(Bool_t fApplyEventVertex) {
    DataTreeCutsConfig::fApplyEventVertex = fApplyEventVertex;
  }

  Int_t GetFittedVertexId() const {
    return fFittedVertexId;
  }

  void SetFittedVertexId(Int_t fFittedVertexId) {
    DataTreeCutsConfig::fFittedVertexId = fFittedVertexId;
  }

  Bool_t IsApplyEventBPD() const {
    return fApplyEventBPD;
  }

  void ApplyEventBPD(Bool_t fApplyEventBPD) {
    DataTreeCutsConfig::fApplyEventBPD = fApplyEventBPD;
  }

  Double_t GetVertexChi2Max() {
    return fVertexChi2Max;
  }

  void SetVertexChi2Max(Double_t value) {
    fVertexChi2Max = value;
  }

  Double_t GetVertexMax(Int_t component) {
    return fVertexLimits[component][1];
  }
  void SetVertexMax(Int_t component, Double_t value) {
    fVertexLimits[component][1] = value;
  }

  Double_t GetVertexMin(Int_t component) {
    return fVertexLimits[component][0];
  }

  void SetVertexMin(Int_t component, Double_t value) {
    fVertexLimits[component][0] = value;
  }

  Double_t GetBPDMax(Int_t bpdId, Int_t component) {
    return fBPDLimits[bpdId][component][1];
  }

  void SetBPDMax(Int_t bpdId, Int_t component, Double_t value) {
    fBPDLimits[bpdId][component][1] = value;
  }

  Double_t GetBPDMin(Int_t bpdId, Int_t component) {
    return fBPDLimits[bpdId][component][0];
  }

  void SetBPDMin(Int_t bpdId, Int_t component, Double_t value) {
    fBPDLimits[bpdId][component][0] = value;
  }

  Bool_t IsApplyTriggers() const {
    return fApplyTriggers;
  }

  void SetApplyTriggers(Bool_t fApplyTriggers) {
    DataTreeCutsConfig::fApplyTriggers = fApplyTriggers;
  }

  Int_t GetS1Min() const {
    return fS1Min;
  }

  void SetS1Min(Int_t fS1Min) {
    DataTreeCutsConfig::fS1Min = fS1Min;
  }

  Int_t GetS1Max() const {
    return fS1Max;
  }

  void SetS1Max(Int_t fS1Max) {
    DataTreeCutsConfig::fS1Max = fS1Max;
  }

  Int_t GetS2Min() const {
    return fS2Min;
  }

  void SetS2Min(Int_t fS2Min) {
    DataTreeCutsConfig::fS2Min = fS2Min;
  }

  Int_t GetS2Max() const {
    return fS2Max;
  }

  void SetS2Max(Int_t fS2Max) {
    DataTreeCutsConfig::fS2Max = fS2Max;
  }

  const Double_t GetSLinearCutParams(Int_t component) const {
    return fSLinearCutParams[component];
  }

  void SetSLinearCutParams(Int_t component, Double_t value) {
    fSLinearCutParams[component] = value;
  }

  Double_t GetNHitsToNHitsPotMin() const {
    return fNHitsToNHitsPotMin;
  }

  Double_t GetNHitsToNHitsPotMax() const {
    return fNHitsToNHitsPotMax;
  }

  void SetNHitsToNHitsPotRange(Double_t min, Double_t max) {
    DataTreeCutsConfig::fNHitsToNHitsPotMin = min;
    DataTreeCutsConfig::fNHitsToNHitsPotMax = max;
  }

  Int_t GetNHitsTotalMin() const {
    return fNHitsTotalMin;
  }

  void SetNHitsTotalMin(Int_t fNHitsTotalMin) {
    DataTreeCutsConfig::fNHitsTotalMin = fNHitsTotalMin;
  }

  const std::vector <int> &GetNHitsPotVTPC1orVTPC2orMTPCMin() const {
    return fNHitsPotVTPC1orVTPC2orMTPCMin;
  }

  void SetNHitsPotVTPC1orVTPC2orMTPCMin(Int_t fNHitsPotVTPC1Min, Int_t fNHitsPotVTPC2Min, Int_t fNHitsPotMTPCMin) {
    DataTreeCutsConfig::fNHitsPotVTPC1orVTPC2orMTPCMin.at(0) = fNHitsPotVTPC1Min;
    DataTreeCutsConfig::fNHitsPotVTPC1orVTPC2orMTPCMin.at(1) = fNHitsPotVTPC2Min;
    DataTreeCutsConfig::fNHitsPotVTPC1orVTPC2orMTPCMin.at(2) = fNHitsPotMTPCMin;
  }

  Double_t GetDCA_X_Center() const {
    return fDCA_X_Center;
  }

  void SetDCA_X_Center(Double_t fDCA_X_Center) {
    DataTreeCutsConfig::fDCA_X_Center = fDCA_X_Center;
  }

  Double_t GetDCA_Y_Center() const {
    return fDCA_Y_Center;
  }

  void SetDCA_Y_Center(Double_t fDCA_Y_Center) {
    DataTreeCutsConfig::fDCA_Y_Center = fDCA_Y_Center;
  }

  Bool_t IsApplyT2T4() const {
    return fApplyT2T4;
  }

  Bool_t IsApplyT2() const {
    return fApplyT2;
  }
  Bool_t IsApplyT4() const {
    return fApplyT4;
  }

  void ApplyT2T4(Bool_t fApplyT2T4) {
    DataTreeCutsConfig::fApplyT2T4 = fApplyT2T4;
  }
  void ApplyT2(Bool_t fApplyT2) {
    DataTreeCutsConfig::fApplyT2 = fApplyT2;
  }
  void ApplyT4(Bool_t fApplyT4) {
    DataTreeCutsConfig::fApplyT4 = fApplyT4;
  }
  Bool_t IsApplyWFA() const {
    return fApplyWFA;
  }

  void ApplyWFA(Bool_t fApplyWFA) {
    DataTreeCutsConfig::fApplyWFA = fApplyWFA;
  }

  Double_t GetWFATimeWindow() const {
    return fWFATimeWindow;
  }

  void SetWFATimeWindow(Double_t fWFATimeWindow) {
    DataTreeCutsConfig::fWFATimeWindow = fWFATimeWindow;
  }

  Int_t GetWFATriggerId() const {
    return fWFATriggerId;
  }

  void SetWFATriggerId(Int_t fWFATriggerId) {
    DataTreeCutsConfig::fWFATriggerId = fWFATriggerId;
  }

  Double_t GetRapidityMin() const {
    return fRapidityWindow[0];
  }

  Double_t GetRapidityMax() const {
    return fRapidityWindow[1];
  }

  void SetRapidityWindow(Double_t yMin, Double_t yMax) {
    throw std::logic_error("Rapidity window cut makes no sense w/o PID. Removed it until we get PID into QA");
  }

  Bool_t IsApplyTrackPzCut() const {
    return fApplyTrackPzCut;
  }
  void ApplyTrackPzCut(Bool_t f_apply_track_pz_cut) {
    fApplyTrackPzCut = f_apply_track_pz_cut;
  }
  Double_t GetPzMin() const {
    return fPzMin;
  }
  void SetPzMin(Double_t f_pz_min) {
    fPzMin = f_pz_min;
  }

  Line *GetEvsMBottom() const {
  		return fEvsMBottom;
  }

  void SetEvsMBottom(Line *evsMBottom) {
  		fEvsMBottom = evsMBottom;
  }



 private:
  /**
   * Track quality cuts
   */
  Bool_t fApplyTrackQuality{kFALSE};
  Bool_t fApplyNumberOfHitsCut{kFALSE};
  Bool_t fApplyTofHitQuality{kFALSE};
  Int_t fNHitsVTPCMin{-9999};
  Int_t fNHitsMTPCMin{-9999};
  Int_t fNHitsPotVTPC1Min{-9999};
  Int_t fNHitsPotVTPC2Min{-9999};
  Int_t fNHitsPotVTPCMin{-9999};
  Int_t fNHitsPotMTPCMin{-9999};
  Int_t fNHitsTotalMin{-9999};
  std::vector <int> fNHitsPotVTPC1orVTPC2orMTPCMin{-9999,-9999,-9999};
  Double_t fNHitsToNHitsPotMin{-9999.};
  Double_t fNHitsToNHitsPotMax{9999.};

  Double_t fTrackChiMax{999.};
  Double_t fTrackChiVtxMax{999.};
  Double_t fDCA_Max{999.};
  Double_t fDCA_X_Max{999.};
  Double_t fDCA_Y_Max{999.};
  Double_t fDCA_X_Center{0.};
  Double_t fDCA_Y_Center{0.};
  float fTofHitMatchRadius{100000.};
  std::vector<float> fTofHitPathLength = {0., 100000.};
  std::vector<float> fTofHitBeta = {0., 100000.};
  std::vector<float> fTofHitTime = {0., 100000.};
  std::vector<float> fTofHitPosition = {0., 100000., 0., 100000., 0., 100000.};

  /**
   * Kinematics
   */

  Bool_t fApplyTrackPzCut{kFALSE};
  Double_t fPzMin{-999};

  Bool_t fApplyTrackKinematics{kFALSE};
  Double_t fPtMin{-999.};
  Double_t fPtMax{999.};
  Double_t fEtaMin{-999.};
  Double_t fEtaMax{999.};
  Double_t fRapidityWindow[2] = {-999., 999.};

  /**
   * Event quality cuts
   */
  Bool_t fApplyEventQuality{kFALSE};
  Bool_t fNonZeroEnergyPSD{kFALSE};
  Int_t fNGoodTracksMin{0};




  /**
   * Vertex
   */
  Bool_t fApplyEventVertex{kFALSE};
  Int_t fFittedVertexId;
  Double_t fVertexChi2Max{-999.};
  Double_t fVertexLimits[3][2]; // first: Component; second: Limits

  /**
   * BPD
   */
  Bool_t fApplyEventBPD{kFALSE};
  Double_t fBPDLimits[3][2][2];  // first: BPD ID; second: Component; third: Limits


  /**
   * Simple Triggers
   */
  Bool_t fApplyTriggers{kFALSE};
  Int_t fS1Min;
  Int_t fS1Max;
  Int_t fS2Min;
  Int_t fS2Max;
  Double_t fSLinearCutParams[3];

  /**
   * T4
   */
  Bool_t fApplyT2T4{kFALSE};
  Bool_t fApplyT2{kFALSE};
  Bool_t fApplyT4{kFALSE};

  /**
   * WFA
   */
  Bool_t fApplyWFA{kFALSE};
  Int_t fWFATriggerId{0};
  Double_t fWFATimeWindow;

  cuts::Line *fEvsMBottom{nullptr};

 ClassDef(cuts::DataTreeCutsConfig, 3)



};

}

#endif //DATATREEQA_DATATREECUTSCONFIG_H
