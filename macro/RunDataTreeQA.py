#!/usr/bin/python

from __future__ import print_function    # (at top of module)

import ROOT
import sys
import argparse

parser = argparse.ArgumentParser(description='Run QA analysis')
parser.add_argument('-i', '--input', dest='input', help='Input root file(s)', nargs='+', required=True)
parser.add_argument('-o', '--output', dest='output', help='Output file', required=True)
parser.add_argument('-c', '--config', dest='config', help='Name of QA configuration', required=True)
parser.add_argument('-cc', '--cuts-config', dest='cuts_config', help='Name of cuts configuration', required=False)
parser.add_argument('--dictionary-location', dest='dict_location', default='./QAConfigurations.root',
                    help='Location of dictionary file with QA configurations')
parser.add_argument('--reference-chain', dest='reference_chain', help='Reference root file(s)', nargs='+', required=False)
args = parser.parse_args()


root_libs = ['libDataTreeQA.so']

for lib in root_libs:
    status = ROOT.gSystem.Load(lib)
    sys.exit(status) if status != 0 else 1;


config_file = args.dict_location
input_file = args.input
output_file = args.output
conf_name = args.config

conf_fd = ROOT.TFile.Open(config_file, 'read')
qa_config = conf_fd.Get(conf_name)

chain = ROOT.TChain('DataTree')
for ff in input_file:
    print('File(s):\t', ff)
    chain.Add(ff)

ref_chain = None
if args.reference_chain is not None:
    print(args.reference_chain)
    ref_chain = ROOT.TChain('DataTree')
    for ff in args.reference_chain:
        print('Reference File(s):\t', ff)
        ref_chain.Add(ff)

print('\nUsing QA config:', qa_config.GetName())

if args.cuts_config is not None:
    cuts_config = conf_fd.Get(args.cuts_config)
    qa_config.SetCutsConfig(cuts_config)

qa = ROOT.qa.DataTreeQA()
qa.Init(chain)
qa.SetQAConfig(qa_config)
if ref_chain is not None:
    qa.SetReferenceChain(ref_chain)
qa.Init_Histograms()
qa.Run(chain.GetEntries())
qa.Write_Histograms(output_file)
