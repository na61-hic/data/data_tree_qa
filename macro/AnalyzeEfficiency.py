#!/usr/bin/python

from __future__ import print_function  # (at top of module)

import argparse
import array

import ROOT
import numpy as np

from enum import IntEnum

from functools import reduce


ROOT.gROOT.SetBatch(True)
ROOT.gStyle.SetOptStat(0)
ROOT.gStyle.SetOptTitle(0)


class Axis(IntEnum):
    PT = 0,
    Y = 1,
    PHI = 2,
    MULT = 3


'''
Simple functions allowing to iterate over histogram bins
without much pain
'''


def taxis_iter(axis, underflow=False, overflow=False):
    i_min = 0 if underflow else 1
    i_max = axis.GetNbins() + (2 if overflow else 1)
    for i in range(i_min, i_max):
        yield (i, axis.GetBinCenter(i))


def th2_iter(histogram, underflow=False, overflow=False):
    for i, x in taxis_iter(histogram.GetXaxis(), underflow, overflow):
        for j, y in taxis_iter(histogram.GetYaxis(), underflow, overflow):
            bin_index = histogram.GetBin(i, j)
            bin_value = histogram.GetBinContent(bin_index)
            yield bin_index, bin_value, i, x, j, y


def th3_iter(histogram, underflow=False, overflow=False):
    for i, x in taxis_iter(histogram.GetXaxis(), underflow, overflow):
        for j, y in taxis_iter(histogram.GetYaxis(), underflow, overflow):
            for k, z in taxis_iter(histogram.GetZaxis(), underflow, overflow):
                bin_index = histogram.GetBin(i, j, k)
                bin_value = histogram.GetBinContent(bin_index)
                yield bin_index, bin_value, i, x, j, y, k, z


def thn_iter(histogram, axes, underflow=False, overflow=False):
    ndim = len(axes)
    ax_iters = list(map(lambda ax: taxis_iter(histogram.GetAxis(ax), underflow, overflow), axes))

    result = []
    while True:
        for i, ax_iter in enumerate(ax_iters):
            # this is the first run
            if len(result) < ndim:
                result.append(ax_iter.__next__())
                continue

            try:
                result[i] = ax_iter.__next__()
                break
            except StopIteration:
                # last axis has ended, no more values
                if i == ndim - 1:
                    raise StopIteration
                # reset this iterator, take the first value, GO NEXT AXIS
                ax_iters[i] = taxis_iter(histogram.GetAxis(axes[i]), underflow, overflow)
                result[i] = ax_iters[i].__next__()

        yield tuple(reduce(lambda x, y: x + y, result))


def clone_and_reset(hist, new_name):
    hist_new = hist.Clone(new_name)
    hist_new.Reset()
    return hist_new


parser = argparse.ArgumentParser(description='Run Efficiency analysis')
parser.add_argument('-i', '--input', dest='input', help='Input root file(s)', required=True)
parser.add_argument('-o', '--output', dest='output', help='Output file', required=True)
parser.add_argument('--acceptance-map', dest='acc_map', help='Acceptance map file', default='acceptance-map/acceptance-map.root')
parser.add_argument('--beam-momentum', dest='momentum', help='Beam Momentum', default=31, type=int)
args = parser.parse_args()

qa_file = ROOT.TFile.Open(args.input)
efficiency_file = ROOT.TFile.Open(args.output, 'recreate')
acceptance_map_file = ROOT.TFile.Open(args.acc_map)

cwd = qa_file.Get('efficiency')
print('CWD = ' + cwd.GetName())

hTracks_hadplus = clone_and_reset(cwd.Get('hTrack_p_pT_Y_phi_Mult'), 'hTrack_hadplus_pT_Y_phi_Mult')
hMCTracks_hadplus = clone_and_reset(hTracks_hadplus, 'hMCTrack_hadplus_pT_Y_phi_Mult')
for hadron in ['piplus', 'kplus']:
    hTracks_hadplus.Add(cwd.Get('hTrack_{}_pT_Y_phi_Mult'.format(hadron)))
    hMCTracks_hadplus.Add(cwd.Get('hMCTrack_{}_pT_Y_phi_Mult'.format(hadron)))
cwd.Add(hTracks_hadplus)
cwd.Add(hMCTracks_hadplus)


acceptance_map = acceptance_map_file.Get(str(args.momentum))
acceptance_map.SetTitle('arXiv:1510.00163 [hep-ex]')


def in_acceptance(pt, ypi, phi):
    phi = 180.0/np.pi*phi

    bb = acceptance_map.FindBin(ypi, phi, pt)
    if acceptance_map.GetBinContent(bb) > 0.0:
        return True
    return False


def get_correction_map(hTracks, hMCTracks, particle, phi_lo=-1.2, phi_hi=1.2):
    x_phi_mesh = np.array([-np.pi, -2.0, -1.0, 1.0, 2.0, np.pi])

    hTracks.Rebin(array.array('i', [4, 4, 4, 1]))
    hMCTracks.Rebin(array.array('i', [4, 4, 4, 1]))

    # Axes
    # 0: pT
    # 1: Y
    # 2: phi
    # 3: multiplicity

    ##############################
    # pT Y Efficiency
    ##############################

    hTracks.GetAxis(2).SetRangeUser(-np.pi, np.pi)
    hMCTracks.GetAxis(2).SetRangeUser(-np.pi, np.pi)

    hTracksPtY = hTracks.Projection(0, 1)
    hMCTracksPtY = hMCTracks.Projection(0, 1)

    hCorrectionMap = hTracksPtY.Clone('hCorrectionMapPtY_Integral')
    hCorrectionMap.Divide(hTracksPtY, hMCTracksPtY)
    hCorrectionMap.SetTitle('[{}]'.format(particle[1]))
    hCorrectionMap.SetXTitle('Y')
    hCorrectionMap.SetYTitle('p_{T}')
    hCorrectionMap.SetMaximum(1.0)

    hCorrectionMap.Write()

    x_phi_lo = x_phi_mesh[:-1]
    x_phi_hi = x_phi_mesh[1:]
    for i_mult, (x_lo, x_hi) in enumerate(zip(x_phi_lo, x_phi_hi)):
        hTracks.GetAxis(2).SetRangeUser(x_lo, x_hi)
        hMCTracks.GetAxis(2).SetRangeUser(x_lo, x_hi)

        hTracksPtY = hTracks.Projection(0, 1)

        hMCTracksPtY = hMCTracks.Projection(0, 1)

        hCorrectionMap = hTracksPtY.Clone('hCorrectionMapPtY_{}_Phi{}'.format(particle[0], str(i_mult + 1)))
        hCorrectionMap.Divide(hTracksPtY, hMCTracksPtY)
        hCorrectionMap.SetTitle('[{}]~~\\phi = ({:.2}, {:.2})'.format(particle[1], x_lo, x_hi))
        hCorrectionMap.SetXTitle('Y - Y_{CM}')
        hCorrectionMap.SetYTitle('p_{T}')

        hCorrectionMap.SetMaximum(1.0)

        hCorrectionMap.Write()

    ##############################
    # pT Y Multiplicity
    ##############################

    # find optimal multiplicity binning
    hMult = hTracks.Projection(3)
    max_mult = hMult.GetBinContent(hMult.GetMaximumBin())
    threshold = 0.1 * max_mult

    mult_min_bin = hMult.FindFirstBinAbove(threshold)
    mult_min = hMult.GetXaxis().GetBinLowEdge(mult_min_bin)
    mult_max_bin = hMult.FindLastBinAbove(threshold)
    mult_max = hMult.GetXaxis().GetBinUpEdge(mult_max_bin)

    mult_step = hMult.Integral(mult_min_bin, mult_max_bin)/5.0

    x_mult_mesh = [mult_min_bin, ]
    for i_mult in range(mult_min_bin, mult_max_bin + 1):
        if hMult.Integral(x_mult_mesh[len(x_mult_mesh) - 1], i_mult) >= mult_step:
            print('Events in bin: ', hMult.Integral(x_mult_mesh[len(x_mult_mesh) - 1], i_mult))
            x_mult_mesh.append(i_mult)

    if hMult.Integral(x_mult_mesh[len(x_mult_mesh) - 1], mult_max_bin) > 0.8*mult_step:
        x_mult_mesh.append(mult_max_bin)
    else:
        print('Omitting last multiplicity bin...')

    x_mult_mesh = list(map(lambda i_mult: hMult.GetXaxis().GetBinLowEdge(i_mult), x_mult_mesh))
    x_mult_mesh = np.array(x_mult_mesh)
    x_mult_lo = x_mult_mesh[:-1]
    x_mult_hi = x_mult_mesh[1:]

    x_phi_lo = phi_lo
    x_phi_hi = phi_hi

    for i_mult, (x_lo, x_hi) in enumerate(zip(x_mult_lo, x_mult_hi)):
        # ================= Pt Y ==================== #
        hTracks.GetAxis(2).SetRangeUser(x_phi_lo, x_phi_hi)
        hMCTracks.GetAxis(2).SetRangeUser(x_phi_lo, x_phi_hi)

        hTracks.GetAxis(3).SetRangeUser(x_lo, x_hi)
        hMCTracks.GetAxis(3).SetRangeUser(x_lo, x_hi)

        hTracksPtY = hTracks.Projection(0, 1)
        hTracksPtY.SetName('hTracksPtY_{}_Mult{}'.format(particle[0], str(i_mult + 1)))
        hMCTracksPtY = hMCTracks.Projection(0, 1)
        hMCTracksPtY.SetName('hMCTracksPtY_{}_Mult{}'.format(particle[0], str(i_mult + 1)))

        hCorrectionMap = hTracksPtY.Clone('hCorrectionMapPtY_{}_Mult{}'.format(particle[0], str(i_mult + 1)))
        hCorrectionMap.Divide(hTracksPtY, hMCTracksPtY)
        hCorrectionMap.SetTitle('[{}]  Multiplicity = ({}, {})'.format(particle[1], x_lo, x_hi))

        hCorrectionMap.SetMaximum(1.0)
        hTracksPtY.Write()
        hMCTracksPtY.Write()
        hCorrectionMap.Write()

        # ================= Pt Phi ====================#
        hTracks.GetAxis(2).SetRangeUser(-np.pi, np.pi)
        hMCTracks.GetAxis(2).SetRangeUser(-np.pi, np.pi)

        hTracksPtPhi = hTracks.Projection(0, 2)
        hTracksPtPhi.SetName('hTracksPtPhi_{}_Mult{}'.format(particle[0], str(i_mult + 1)))
        hMCTracksPtPhi = hMCTracks.Projection(0, 2)
        hMCTracksPtPhi.SetName('hMCTracksPtPhi_{}_Mult{}'.format(particle[0], str(i_mult + 1)))

        hCorrectionMap = hTracksPtPhi.Clone('hCorrectionMapPtPhi_{}_Mult{}'.format(particle[0], str(i_mult + 1)))
        hCorrectionMap.Divide(hTracksPtPhi, hMCTracksPtPhi)
        hCorrectionMap.SetTitle('[{}]  Multiplicity = ({}, {})'.format(particle[1], x_lo, x_hi))

        hCorrectionMap.SetMaximum(1.0)
        hTracksPtPhi.Write()
        hMCTracksPtPhi.Write()
        hCorrectionMap.Write()

        # ================= Y Phi ====================#
        hTracksYPhi = hTracks.Projection(1, 2)
        hTracksYPhi.SetName('hTracksYPhi_{}_Mult{}'.format(particle[0], str(i_mult + 1)))
        hMCTracksYPhi = hMCTracks.Projection(1, 2)
        hMCTracksYPhi.SetName('hMCTracksYPhi_{}_Mult{}'.format(particle[0], str(i_mult + 1)))

        hCorrectionMap = hTracksYPhi.Clone('hCorrectionMapYPhi_{}_Mult{}'.format(particle[0], str(i_mult + 1)))
        hCorrectionMap.Divide(hTracksYPhi, hMCTracksYPhi)
        hCorrectionMap.SetTitle('[{}]  Multiplicity = ({}, {})'.format(particle[1], x_lo, x_hi))

        hCorrectionMap.SetMaximum(1.0)
        hTracksYPhi.Write()
        hMCTracksYPhi.Write()
        hCorrectionMap.Write()

        # ================= Efficiency ==============#
        hTracksPtYPhi = hTracks.Projection(3, array.array('i', [Axis.PT, Axis.Y, Axis.PHI]))
        hMCTracksPtYPhi = hMCTracks.Projection(3, array.array('i', [Axis.PT, Axis.Y, Axis.PHI]))

        hEfficiency = hTracksPtYPhi.Clone('hEfficiency_Mult{}'.format(str(i_mult+1)))
        hEfficiency.Divide(hTracksPtYPhi, hMCTracksPtYPhi)

        hEfficiencyFlat = ROOT.TH1D("hEfficiencyFlat_Mult{}".format(str(i_mult+1)), "", 100, 0, 3)
        hEfficiencyFlat.SetTitle('{}    Efficiency | Multiplicity = ({}, {})'.format(particle[1], x_lo, x_hi))
        hEfficiencyFlat.SetXTitle('Efficiency')

        # shifts phi by pi for negatively charged particles only
        def shift_phi(phi):
            if particle[0].endswith('minus'):
                return (np.pi if phi >= 0 else - np.pi) - phi
            return phi

        for i, pt, j, ypi, k, phi in thn_iter(hEfficiency, [Axis.PT, Axis.Y, Axis.PHI]):
            if in_acceptance(pt, ypi, shift_phi(phi)):
                efficiency = hEfficiency.GetBinContent(hEfficiency.GetBin(array.array('i', [i, j, k])))
                mc_tracks = hMCTracksPtYPhi.GetBinContent(hEfficiency.GetBin(array.array('i', [i, j, k])))
                if efficiency > 0 and mc_tracks >= 5:
                    hEfficiencyFlat.Fill(efficiency)

        #
        # Populate and draw acceptance map
        #
        hAcceptanceMap = clone_and_reset(acceptance_map, 'hAcceptanceMap_Mult{}'.format(str(i_mult + 1)))
        hAcceptanceMap.SetTitle('eff > 90%')
        hAcceptanceMap.SetMarkerColor(ROOT.kBlue)
        hAcceptanceMap.SetMarkerStyle(ROOT.kFullCircle)

        for bin_index, bin_value, i, ypi, j, phi, k, pt in th3_iter(hAcceptanceMap):
            efficiency = hEfficiency.GetBinContent(hEfficiency.GetBin(array.array('d', [pt, ypi, np.pi*phi/180])))
            if efficiency > 0.9:
                hAcceptanceMap.Fill(ypi, phi, pt)

        #
        # Populate and draw efficiency map in different projections
        # TODO make it function
        hEfficiencyPtY = clone_and_reset(hEfficiency.Projection(Axis.PT, Axis.Y), 'hEfficiencyPtY_Mult{}'.format(str(i_mult + 1)))
        for i, pt, j, ypi in thn_iter(hEfficiency, [Axis.PT, Axis.Y]):
            val_arr = np.array([
                (hEfficiency.GetBinContent(hEfficiency.GetBin(array.array('i', [i, j, k])))
                 if in_acceptance(pt, ypi, shift_phi(phi)) else 0.0)
                for k, phi in taxis_iter(hEfficiency.GetAxis(Axis.PHI))])

            val = np.mean(val_arr[np.nonzero(val_arr)])
            hEfficiencyPtY.SetBinContent(hEfficiencyPtY.GetBin(j, i), val if not np.isnan(val) else 0.0)

        hEfficiencyPtPhi = clone_and_reset(hEfficiency.Projection(Axis.PT, Axis.PHI), 'hEfficiencyPtPhi_Mult{}'.format(str(i_mult + 1)))
        for i, pt, k, phi in thn_iter(hEfficiency, [Axis.PT, Axis.PHI]):
            val_arr = np.array([
                (hEfficiency.GetBinContent(hEfficiency.GetBin(array.array('i', [i, j, k])))
                 if in_acceptance(pt, ypi, shift_phi(phi)) else 0.0)
                for j, ypi in taxis_iter(hEfficiency.GetAxis(Axis.Y))])

            val = np.mean(val_arr[np.nonzero(val_arr)])
            hEfficiencyPtPhi.SetBinContent(hEfficiencyPtPhi.GetBin(k, i), val if not np.isnan(val) else 0.0)

        hEfficiencyYPhi = clone_and_reset(hEfficiency.Projection(Axis.Y, Axis.PHI), 'hEfficiencyYPhi_Mult{}'.format(str(i_mult + 1)))
        for j, ypi, k, phi in thn_iter(hEfficiency, [Axis.Y, Axis.PHI]):
            val_arr = np.array([
                (hEfficiency.GetBinContent(hEfficiency.GetBin(array.array('i', [i, j, k])))
                 if in_acceptance(pt, ypi, shift_phi(phi)) else 0.0)
                for i, pt in taxis_iter(hEfficiency.GetAxis(Axis.PT))])

            val = np.mean(val_arr[np.nonzero(val_arr)])
            hEfficiencyYPhi.SetBinContent(hEfficiencyYPhi.GetBin(k, j), val if not np.isnan(val) else 0.0)


        hAcceptanceMap.Write()

        hEfficiencyPtY.SetMaximum(1.0)
        hEfficiencyPtY.Write()
        hEfficiencyPtPhi.SetMaximum(1.0)
        hEfficiencyPtPhi.Write()
        hEfficiencyYPhi.SetMaximum(1.0)
        hEfficiencyYPhi.Write()
        hEfficiencyFlat.Write()


for particle in [
    ('hadplus', 'h^{+}'),
    ('p', 'p'),
    ('piplus', '\\pi^{+}'),
    ('piminus', '\\pi^{-}'),
]:


    phi_lo = -np.pi
    phi_hi = np.pi

    hTracks = cwd.Get('hTrack_{}_pT_Y_phi_Mult'.format(particle[0]))
    hMCTracks = cwd.Get('hMCTrack_{}_pT_Y_phi_Mult'.format(particle[0]))

    efficiency_file.mkdir(particle[0]).cd()
    get_correction_map(hTracks, hMCTracks, particle, phi_lo, phi_hi)

for particle in [
    ('hadplus', 'h^{+}'),
    ('hadminus', 'h^{-}'),
]:
    qa_file.Get('efficiency').cd()

    hTracks = cwd.Get('hTrack_{}_pT_Eta_phi_Mult'.format(particle[0]))
    hTracksPtEtaMult = hTracks.Projection(0, 1, 3)
    hMCTracks = cwd.Get('hMCTrack_{}_pT_Eta_phi_Mult'.format(particle[0]))
    hMCTracksPtEtaMult = hMCTracks.Projection(0, 1, 3)

    hCorrectionMap = clone_and_reset(hTracksPtEtaMult, 'hCorrectionMap_{}'.format(particle[0]))
    hCorrectionMap.SetTitle(particle[1])
    hCorrectionMap.Divide(hTracksPtEtaMult, hMCTracksPtEtaMult)

    efficiency_file.cd()
    hCorrectionMap.Write()



print('File ', args.output, ' was successfully written')
