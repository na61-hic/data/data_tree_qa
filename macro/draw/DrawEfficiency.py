#!/usr/bin/python

import ROOT
import sys

efficiency = ROOT.TFile.Open(sys.argv[1])

particle = sys.argv[2]
efficiency.Get(particle).cd()

ROOT.gDirectory.ls()

ROOT.gStyle.SetOptStat(0)

c1 = ROOT.TCanvas()
c1.Divide(3, 2)

for i in range(1,6):
    hist = ROOT.gDirectory.FindObjectAny('hCorrectionMapPtY_{}_Phi{}'.format(particle, i))
    
    c1.cd(i)
    hist.Draw('colz')

c1.cd(6)
hist = ROOT.gDirectory.FindObjectAny('hCorrectionMapPtY_Integral')
hist.Draw('colz')


c2 = ROOT.TCanvas()
c2.Divide(3, 2)

projections = ['PtY', 'PtPhi', 'YPhi']
mult_slices = ['1', '4' ]

i = 1
for ss in mult_slices:
    for proj in projections:
        c2.cd(i)
        hist = ROOT.gDirectory.FindObjectAny('hCorrectionMap{}_{}_Mult{}'.format(proj, particle, ss))
        hist.Draw('colz')
        i+=1

input()
