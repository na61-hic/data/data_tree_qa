#!/usr/bin/python

from __future__ import print_function    # (at top of module)

import ROOT
import sys
import argparse

parser = argparse.ArgumentParser(description='Run QA analysis')
parser.add_argument('-i', '--input', dest='input', help='Input root file(s)', required=True)
parser.add_argument('-o', '--output', dest='output', help='Output file', required=True)
args = parser.parse_args()

qa_file = ROOT.TFile.Open(args.input)

rbr_d = qa_file.Get('run_by_run')

hNEventsInRun = rbr_d.Get('hNEventsInRun')

first_bin = hNEventsInRun.FindFirstBinAbove(0.0)
last_bin = hNEventsInRun.FindLastBinAbove(0.0)

ROOT.gStyle.SetOptStat(0)
c1 = ROOT.TCanvas("c1","sub data",200,10,700,500);
c1.Print(args.output + '(', 'pdf')

rbr_d.ReadAll()
for obj in rbr_d.GetList():
    obj.SetTitle(obj.GetName())
    obj.GetXaxis().SetRange(first_bin, last_bin)
    obj.Draw()
    c1.Print(args.output, 'pdf')

c1.Print(args.output + ')', 'pdf')




input()
