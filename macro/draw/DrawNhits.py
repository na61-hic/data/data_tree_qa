#!/usr/bin/python

import ROOT
import sys
import re

data = ROOT.TFile.Open(sys.argv[1])
mc = ROOT.TFile.Open(sys.argv[2])


data_nevents = data.FindObjectAny('hMreco').GetEntries()
mc_nevents = mc.FindObjectAny('hMreco').GetEntries()

#
# Hits individual TPC
#
hist_names = ['hNhits_VTPC1', 'hNhits_VTPC2', 'hNhits_MTPC', 'hNhitsPot_VTPC1', 'hNhitsPot_VTPC2', 'hNhitsPot_MTPC']

ROOT.gStyle.SetOptStat(0)

c1 = ROOT.TCanvas()
c1.Divide(3,2)
for i, name in enumerate(hist_names):
    hist_data = data.FindObjectAny(name)
    hist_mc = mc.FindObjectAny(name)

    search = re.search('^hNhits(Pot)?_(.*)$', name, re.I | re.M)
    title = 'N_{} {}'.format('{hits possible}' if search.group(1) is not None else '{hits}',search.group(2))

    xmax = 150 if search.group(2) == 'MTPC' else 100

    det = search.group(2)
    

    x1 = hist_mc.GetXaxis().FindBin(10.0)
    x2 = hist_mc.GetXaxis().FindBin(xmax)

    # hist_mc.Scale(hist_data.Integral(x1, x2)/hist_mc.Integral(x1, x2))
    hist_mc.Scale(data_nevents*1.0/mc_nevents)

    c1.cd(i+1)
    ROOT.gPad.SetLogy()
    ROOT.gPad.SetLogy()
    
    hist_data.SetLineColor(ROOT.kRed)
    hist_data.SetLineWidth(2)
    hist_data.SetTitle(title)
    hist_mc.SetLineWidth(2)
    hist_mc.SetLineColor(ROOT.kBlack)
    hist_mc.SetFillColor(ROOT.kBlack)
    hist_mc.SetFillStyle(3006)
    hist_data.GetXaxis().SetRangeUser(0, xmax)
    hist_data.Draw()
    hist_mc.Draw('pfc hist same')

    legend = ROOT.TLegend(0.1, 0.1, 0.4, 0.4)
    legend.AddEntry(hist_data, 'data')
    legend.AddEntry(hist_mc, 'simulation')
    legend.Draw()

#
# Hits ALL TPC
#
hist_names = ['hNhits_allTPC', 'hNhitsPot_allTPC']

ROOT.gStyle.SetOptStat(0)

c3 = ROOT.TCanvas()
c3.Divide(1,2)
for i, name in enumerate(hist_names):
    hist_data = data.FindObjectAny(name)
    hist_mc = mc.FindObjectAny(name)

    search = re.search('^hNhits(Pot)?_(.*)$', name, re.I | re.M)
    title = 'N_{} {}'.format('{hits possible}' if search.group(1) is not None else '{hits}', 'VTPC1 + VTPC2 + MTPC')

    xmax = 260

    det = search.group(2)
    

    x1 = hist_mc.GetXaxis().FindBin(30.0)
    x2 = hist_mc.GetXaxis().FindBin(xmax)

    hist_mc.Scale(data_nevents*1.0/mc_nevents)
    
    c3.cd(i+1)
    
    hist_data.SetLineColor(ROOT.kRed)
    hist_data.SetLineWidth(2)
    hist_data.SetTitle(title)
    hist_mc.SetLineWidth(2)
    hist_mc.SetLineColor(ROOT.kBlack)
    hist_mc.SetFillColor(ROOT.kBlack)
    hist_mc.SetFillStyle(3006)
    hist_data.GetXaxis().SetRangeUser(0, xmax)
    hist_data.Draw('same')
    hist_mc.Draw('pfc hist same')

c3.cd(1)
legend = ROOT.TLegend(0.1, 0.1, 0.4, 0.4)
legend.AddEntry(hist_data, 'data')
legend.AddEntry(hist_mc, 'simulation')
legend.Draw()



#
# Multiplicity
#

hMreco_name = 'hMreco'

hist_data = data.FindObjectAny(hMreco_name)
hist_mc = mc.FindObjectAny(hMreco_name)

hist_data.SetLineColor(ROOT.kRed)
hist_data.SetLineWidth(2)
hist_mc.SetLineColor(ROOT.kBlack)
hist_mc.SetFillColor(ROOT.kBlack)
hist_mc.SetFillStyle(3006)
hist_mc.SetLineWidth(2)
 
x1 = int(hist_data.GetXaxis().FindBin(20.0))
x2 = int(hist_data.GetXaxis().GetXmax())

hist_mc.Scale(data_nevents*1.0/mc_nevents)

c2 = ROOT.TCanvas()

hist_data.Draw()
hist_mc.Draw('pfc hist same')

#
# Multiplicity
#

hMreco_name = 'hChi2Ndf'

hist_data = data.FindObjectAny(hMreco_name)
hist_mc = mc.FindObjectAny(hMreco_name)

hist_data.SetLineColor(ROOT.kRed)
hist_data.SetLineWidth(2)
hist_mc.SetLineColor(ROOT.kBlack)
hist_mc.SetFillColor(ROOT.kBlack)
hist_mc.SetFillStyle(3006)
hist_mc.SetLineWidth(2)

x1 = int(hist_data.GetXaxis().FindBin(0.0))
x2 = int(hist_data.GetXaxis().FindBin(10.0))

hist_mc.Scale(data_nevents*1.0/mc_nevents)

c2 = ROOT.TCanvas()

hist_data.Draw()
hist_mc.Draw('pfc hist same')

legend = ROOT.TLegend(0.1, 0.1, 0.4, 0.4)
legend.AddEntry(hist_data, 'data')
legend.AddEntry(hist_mc, 'mc')
legend.Draw()



input()
    
