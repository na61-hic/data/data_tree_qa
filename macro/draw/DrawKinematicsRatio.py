#!/usr/bin/python

import ROOT
import sys

data = ROOT.TFile.Open(sys.argv[1])
mc = ROOT.TFile.Open(sys.argv[2])

hist_names = ['hTrackPhiEta', 'hTrackPhiPt', 'hTrackEtaPt']


c = ROOT.TCanvas('c_Kinematics')
c.Divide(3, 1)

for i, name in enumerate(hist_names):
    hist_data = data.FindObjectAny(name)
    hist_mc = mc.FindObjectAny(name)

    c.cd(i+1)
    ROOT.gPad.SetLogz()


    hist_mc.Scale(1.0*hist_data.GetEntries()/hist_mc.GetEntries())

    hist_ratio = hist_mc.Rebin2D(1, 1, hist_mc.GetName() + '_ratio')
    hist_ratio.SetTitle('MC/data')
    hist_ratio.Divide(hist_data.Rebin2D(1, 1, hist_mc.GetName() + '_temp'))
    hist_ratio.Draw('colz')

input()
