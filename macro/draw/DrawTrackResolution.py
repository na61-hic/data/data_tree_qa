#!/usr/bin/python

import ROOT
import sys


rcwd = ROOT.gDirectory

ROOT.gStyle.SetOptStat(0)
ROOT.gStyle.SetOptTitle(0)

input_file = ROOT.TFile.Open(sys.argv[1])
input_file.cd()


def rebin(hist, n = 10):
    return hist.Rebin(n, hist.GetName() + "_rebinned")

#
# Pt
#

pTrackResolutionPt = rcwd.FindObjectAny("pTrackResolutionPt")
pTrackResolutionPositivePt = rcwd.FindObjectAny("pTrackResolutionPositivePt")
pTrackResolutionNegativePt = rcwd.FindObjectAny("pTrackResolutionNegativePt")


c_PtResolution = ROOT.TCanvas("c_PtResolution")
c_PtResolution.cd()
c_PtResolution.SetLogy()

pTrackResolutionPt.SetTitle("All Particles")
pTrackResolutionPt.SetXTitle("p_{T}")
pTrackResolutionPt.SetYTitle("<|\\Delta p_{T}|/p_{T} >")
pTrackResolutionPt.SetLineColor(ROOT.kBlack)
pTrackResolutionPt.SetMarkerStyle(23)
pTrackResolutionPt.Rebin(10).Draw()

pTrackResolutionPositivePt.SetTitle("Positive")
pTrackResolutionPositivePt.SetLineColor(ROOT.kCyan)
pTrackResolutionPositivePt.SetMarkerStyle(23)
pTrackResolutionPositivePt.SetMarkerColor(ROOT.kCyan)
pTrackResolutionPositivePt.Rebin(10).Draw("same")

pTrackResolutionNegativePt.SetTitle("Negative")
pTrackResolutionNegativePt.SetLineColor(ROOT.kMagenta)
pTrackResolutionNegativePt.SetMarkerStyle(23)
pTrackResolutionNegativePt.SetMarkerColor(ROOT.kMagenta)
pTrackResolutionNegativePt.Rebin(10).Draw("same")

ROOT.gPad.BuildLegend()


input_file.cd()

pTrackResolutionEta = rcwd.FindObjectAny("pTrackResolutionEta")
pTrackResolutionPositiveEta = rcwd.FindObjectAny("pTrackResolutionPositiveEta")
pTrackResolutionNegativeEta = rcwd.FindObjectAny("pTrackResolutionNegativeEta")


c_EtaResolution = ROOT.TCanvas("c_EtaResolution")
c_EtaResolution.cd()
c_EtaResolution.SetLogy()

pTrackResolutionEta.SetTitle("All Particles")
pTrackResolutionEta.SetXTitle("\\eta")
pTrackResolutionEta.SetYTitle("\\Delta \\eta")
pTrackResolutionEta.SetLineColor(ROOT.kBlack)
pTrackResolutionEta.SetMarkerStyle(23)
pTrackResolutionEta.SetMarkerColor(ROOT.kBlack)
pTrackResolutionEta.GetYaxis().SetRangeUser(1e-2, 1e1)
rebin(pTrackResolutionEta).Draw()

pTrackResolutionPositiveEta.SetTitle("Positive")
pTrackResolutionPositiveEta.SetLineColor(ROOT.kCyan)
pTrackResolutionPositiveEta.SetMarkerStyle(23)
pTrackResolutionPositiveEta.SetMarkerColor(ROOT.kCyan)
rebin(pTrackResolutionPositiveEta).Draw("same")

pTrackResolutionNegativeEta.SetTitle("Negative")
pTrackResolutionNegativeEta.SetLineColor(ROOT.kMagenta)
pTrackResolutionNegativeEta.SetMarkerStyle(23)
pTrackResolutionNegativeEta.SetMarkerColor(ROOT.kMagenta)
rebin(pTrackResolutionNegativeEta).Draw("same")

ROOT.gPad.BuildLegend()
c_EtaResolution.Update()

input_file.cd()

pTrackResolutionPhi = rcwd.FindObjectAny("pTrackResolutionPhi")
pTrackResolutionPositivePhi = rcwd.FindObjectAny("pTrackResolutionPositivePhi")
pTrackResolutionNegativePhi = rcwd.FindObjectAny("pTrackResolutionNegativePhi")


c_PhiResolution = ROOT.TCanvas("c_PhiResolution")
c_PhiResolution.cd()
c_PhiResolution.SetLogy()

pTrackResolutionPhi.SetTitle("All Particles")
pTrackResolutionPhi.SetXTitle("\\phi")
pTrackResolutionPhi.GetYaxis().SetRangeUser(1e-2, 1.0)
pTrackResolutionPhi.SetYTitle("\\Delta \\phi")
pTrackResolutionPhi.SetLineColor(ROOT.kBlack)
pTrackResolutionPhi.SetMarkerColor(ROOT.kBlack)
pTrackResolutionPhi.SetMarkerStyle(23)
rebin(pTrackResolutionPhi).Draw()

pTrackResolutionPositivePhi.SetTitle("Positive")
pTrackResolutionPositivePhi.SetLineColor(ROOT.kCyan)
pTrackResolutionPositivePhi.SetMarkerColor(ROOT.kCyan)
pTrackResolutionPositivePhi.SetMarkerStyle(23)
rebin(pTrackResolutionPositivePhi).Draw("same")


pTrackResolutionNegativePhi.SetTitle("Negative")
pTrackResolutionNegativePhi.SetLineColor(ROOT.kMagenta)
pTrackResolutionNegativePhi.SetMarkerColor(ROOT.kMagenta)
pTrackResolutionNegativePhi.SetMarkerStyle(23)
rebin(pTrackResolutionNegativePhi).Draw("same")

ROOT.gPad.BuildLegend()
c_PhiResolution.Update()

input()
