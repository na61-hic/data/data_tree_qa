## DISCLAIMER

I tried to separate DataTree from QA package. Now as you can see ain't no DataTree directory in a project tree.

## INSTALLATION

### DataTree

Firstly you are invited to install `DataTree` package. Better to use ROOT v6 and CMake ~3.

Install DataTree with full procedure including `cmake ... && make && make install`. Specify prefix directory with -DCMAKE_INSTALL_PREFIX option of CMake. After installation you'll find the following structure in prefix directory

```
.
├── include
│   ├── DataTreeBPD.h
│   ├── DataTreeConstants.h
│   ├── DataTreeEvent.h
│   ├── DataTreeEventLinkDef.h
│   ├── datatree_export.h
│   ├── DataTreeMCTrack.h
│   ├── DataTreePSDModule.h
│   ├── DataTreePSDSection.h
│   ├── DataTreeTOFHit.h
│   ├── DataTreeTrack.h
│   ├── DataTreeTrackMatch.h
│   ├── DataTreeTrackParams.h
│   ├── DataTreeTrigger.h
│   ├── DataTreeV0Candidate.h
│   └── DataTreeWFA.h
└── lib
    ├── cmake
    │   └── DataTree
    │       ├── DataTreeConfig.cmake
    │       ├── DataTreeConfigVersion.cmake
    │       ├── DataTreeTargets.cmake
    │       ├── DataTreeTargets-debug.cmake
    │       └── DataTreeTargets-noconfig.cmake
    ├── libDataTree_rdict.pcm
    ├── libDataTree.so -> libDataTree.so.1
    └── libDataTree.so.1
```

This is OK.

## DataTreeQA

Now you are ready to build QA.
```
$ export DATATREE_HOME=/path/to/your/DataTree/installation
$ mkdir build
$ cd build
$ cmake -DEXPERIMENT=<option you chose for compiling DataTree> ../
$ make
```
And it should work
