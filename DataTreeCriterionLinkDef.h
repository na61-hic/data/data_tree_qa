//
// Created by eugene on 3/4/18.
//

#ifdef __CINT__

#pragma link C++ namespace criterion;
#pragma link C++ class criterion::DataTreeEventCriterion+;
#pragma link C++ class criterion::DataTreeBinaryCriterion+;
#pragma link C++ class criterion::DataTreeUnaryCriterion+;
#pragma link C++ class criterion::DataTreeBinaryAnd+;
#pragma link C++ class criterion::DataTreeBinaryOr+;
#pragma link C++ class criterion::DataTreeUnaryNot+;
#pragma link C++ class criterion::DataTreeAlwaysTrue+;
#pragma link C++ class criterion::DataTreeEventCriterionFunction+;

// Utils
#pragma link C++ function criterion::StoreWithName(criterion::DataTreeEventCriterion&, const char*);

#endif
