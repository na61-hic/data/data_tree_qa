//
// Created by eugene on 7/16/19.
//


#include <TFile.h>
#include <TError.h>
#include <THnSparse.h>
#include <TH2D.h>

#include <iostream>
#include <functional>
#include <TCanvas.h>

using namespace std;

enum EAxes : int {
  kPT = 0,
  kY,
  kPHI,
  kMULT
};

struct Axis_st {
  int id;
  string name{""};
  string displayName{""};
  string unit{""};
} const gAxes[] {
    {kPT, "Pt", "p_{T}", "GeV/c"},
    {kY, "Y", "#it{y}_{CM}", ""},
    {kPHI, "Phi", "#phi", "rad"},
    {kMULT, "Mult", "Multiplicity", ""},
};

typedef function<bool(const vector<double> &, double)> Cut;

void forEachBin(THnSparseD *h, function<void(long, const vector<int> &, double)> ff) {

  std::vector<int> binCoords(h->GetNdimensions());
  long binId{-1};

  THnIter iter(h, true);

  while ((binId = iter.Next(binCoords.data())) != -1) {
    double binContent = h->GetBinContent(binId);
    ff(binId, binCoords, binContent);
  }
}

void getBinCenters(THnSparseD *h, const vector<int> &binCts, vector<double> &cts) {
  cts.resize(h->GetNdimensions());

  for (int iDim = 0; iDim < h->GetNdimensions(); ++iDim) {
    cts[iDim] = h->GetAxis(iDim)->GetBinCenter(binCts[iDim]);
  }
}

TH2D *getProjection2D(THnSparseD *h, int axX, int axY, const vector<Cut>& cuts = {}) {

  auto projection = new TH2D(
      "h2temp",
      "",
      h->GetAxis(axX)->GetNbins(),
      h->GetAxis(axX)->GetXmin(),
      h->GetAxis(axX)->GetXmax(),
      h->GetAxis(axY)->GetNbins(),
      h->GetAxis(axY)->GetXmin(),
      h->GetAxis(axY)->GetXmax()
  );

  vector<double> cts{};
  forEachBin(h, [=,&cts](long binId, const vector<int> &binCts, double content) {
    int ctX = binCts[axX];
    int ctY = binCts[axY];
    getBinCenters(h, binCts, cts);

    bool cutsPassed{true};
    for (auto &cut : cuts) {
      if (!cut(cts, content)) {
        cutsPassed = false;
        break;
      }
    }

    if (cutsPassed) {
      projection->SetBinContent(ctX, ctY, projection->GetBinContent(ctX, ctY) + content);
    }
  });

  projection->Sumw2();
  return projection;
}

int main(int argc, char **argv) {

  auto inputFile = shared_ptr<TFile>(TFile::Open("./qa.root", "read"));

  if (!inputFile) {
    Error(__func__, "Unable to open file");
  }

  TFile outputFile("efficiency.root", "recreate");

  Info(__func__, "Input: %s", inputFile->GetName());

  vector <string > particles {
    "p", "piplus", "piminus"
  };

  for (const auto &particle : particles) {

    THnSparseD *hTracksPtYPhiMult = nullptr;
    inputFile->GetObject(Form("efficiency/hTrack_%s_pT_Y_phi_Mult", particle.c_str()), hTracksPtYPhiMult);
    THnSparseD *hMCTracksPtYPhiMult = nullptr;
    inputFile->GetObject(Form("efficiency/hMCTrack_%s_pT_Y_phi_Mult", particle.c_str()), hMCTracksPtYPhiMult);

    if (!(hTracksPtYPhiMult && hMCTracksPtYPhiMult)) {
      continue;
    }

    auto outputDir = outputFile.mkdir(particle.c_str());



    const vector<pair<int, int> > axesCombinations{
        {kY, kPT},
        {kPHI, kPT},
        {kPHI, kY}
    };

    if (hTracksPtYPhiMult) {
      for (auto const &[axX, axY] : axesCombinations) {
        auto hPtVsY = unique_ptr<TH2D>(getProjection2D(hTracksPtYPhiMult, axX, axY));
        auto hMCPtVsY = unique_ptr<TH2D>(getProjection2D(hMCTracksPtYPhiMult, axX, axY));
        auto ratio = (*hPtVsY) / (*hMCPtVsY);

        ratio.SetMinimum(0.);
        ratio.SetMaximum(1.2);
        outputDir->WriteObject(&ratio, Form("hCorrectionMap_%sVs%s",
                                            gAxes[axY].name.c_str(),
                                            gAxes[axX].name.c_str()));
      }
    }
  }

  return 0;
}

